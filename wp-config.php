<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gordon');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gKBy4;Z @-6F|r,_7-*^)^*w_s]-6Rj]%ll[|.1Qd6++#/`l`<3QM|`F%v:b^G{d');
define('SECURE_AUTH_KEY',  'z3SgNG.ZoY[>&KW wHs.p)#)t&A)L|0jQ|GK2FgpRd]{$jm$.F{fB4c7mdCj&dFk');
define('LOGGED_IN_KEY',    'zqV!&Wp8-s%ix&U-|XSZ!*uCEFe8dt$TK%+x3g Gr1Hu:`p&W*@7E=b>C[s+F$K|');
define('NONCE_KEY',        'ukfmE:(7`#U}lC_GvhgA{tze?PV]~P9*Kw(sY,2Mr_rcl:)O+xH(r^`$)brJdTc/');
define('AUTH_SALT',        '|0,gc9MOR&`ke(h9~R|A<:mzO-U<>0gcsM?WZ*|dXp&Z<d>[@[;gY|<!HXFVmN?v');
define('SECURE_AUTH_SALT', '!<`I?z`T1;qp-qroj@/8@3+,y9.@qcI<et,{80CU|vx,`Jk+(#3`lg7$[?mN^?oG');
define('LOGGED_IN_SALT',   ')A?[<!tYIF<+!Gb+6ljI(|xi0N]-=k?oX|GpMQw8&]N|.%+Gia2Rqg!@1k$I@AO ');
define('NONCE_SALT',       '~_&,n0e2Q=DUYRzG7a]XXqNP>/zAiyU-|0?hrRj+0ytx`~z?u4.Ds%vg32XRgj_2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
