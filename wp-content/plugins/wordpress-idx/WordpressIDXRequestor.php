<?php
if( !class_exists('wordpressIDXRequestor')){
	class wordpressIDXRequestor{
				
		public static function remoteRequest( $wpidxUrl, $ajaxRequest=false ){
			wordpressIDXLogger::getInstance()->debug("Begin wordpressIDXRequestor.remoteRequest: " );
				
			//We don't try to get subscriber information for ajax requests
			//because of cookie related complications.
			if( !strpos(strtolower($wpidxUrl), "subscriberid=") && !$ajaxRequest ){
				$subscriber = wordpressIDXStateManager::getInstance()->getCurrentSubscriber();

				if( !is_null($subscriber) && '' != $subscriber){
					$subscriberId=$subscriber->getId();
					wordpressIDXLogger::getInstance()->debug('subscriberId: ' . $subscriberId );
					$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId );
				}
			}
						
			//If the url does not have the lead capture id then try to add it
			$wpidxUrlHasLeadCapture=strrpos($wpidxUrl, "leadCaptureId=");
			if($wpidxUrlHasLeadCapture === false){
				$leadCaptureId = wordpressIDXStateManager::getInstance()->getLeadCaptureId();
				if( !is_null($leadCaptureId) && '' != $leadCaptureId){
					wordpressIDXLogger::getInstance()->debug('leadCaptureId: ' . $leadCaptureId );
					$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "leadCaptureId", $leadCaptureId );
				}
			}
				
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "version", wordpressIDXConstants::VERSION );
			$userAgent=$_SERVER['HTTP_USER_AGENT'];
			if( $userAgent != null ){
				$userAgent=urlencode($userAgent);
				$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "uagent", $userAgent ) ;	
			}
			
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "loadJQuery", "false" ) ;
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "leadCaptureSupport", "true" ) ;
			
			wordpressIDXLogger::getInstance()->debug("wpidxUrl: " . $wpidxUrl);
			$wpidxid=site_url() + ";" + "WordpressPlugin";
			$requestArgs = array("timeout"=>"20", "wpidxid"=> $wpidxid );
			wordpressIDXLogger::getInstance()->debug("before request");
			$response = wp_remote_get($wpidxUrl, $requestArgs);
			wordpressIDXLogger::getInstance()->debug("after request");
			
			if( is_wp_error($response)){
				$contentInfo=null;
			}
			else{
				$responseBody = wp_remote_retrieve_body( $response );
				wordpressIDXLogger::getInstance()->debug('responseBody: ' . $responseBody );
				$contentInfo=json_decode($responseBody);
			}
			wordpressIDXLogger::getInstance()->debug("after get body");
				
			//Save the leadCaptureId, if we get it back.
			if( isset( $contentInfo->leadCaptureId ) ){
				wordpressIDXStateManager::getInstance()->saveLeadCaptureId($contentInfo->leadCaptureId);
			}
			
			if( isset( $contentInfo->searchContext ) ){
				wordpressIDXStateManager::getInstance()->setSearchContext($contentInfo->searchContext);
			}			
				
			if( !wordpressIDXRequestor::isError($contentInfo) && isset( $contentInfo->subscriberInfo )){
				$subscriberData=$contentInfo->subscriberInfo ;
				$subscriberInfo=wordpressIDXSubscriber::getInstance($subscriberData->subscriberId,$subscriberData->name, $subscriberData->email );
				wordpressIDXStateManager::getInstance()->saveSubscriberLogin($subscriberInfo);
			}
			
			if( !wordpressIDXRequestor::isError($contentInfo) && isset( $contentInfo->searchSummary )){
				$searchSummary=$contentInfo->searchSummary ;
				wordpressIDXStateManager::getInstance()->saveSearchSummary($searchSummary);
			}
				
			wordpressIDXLogger::getInstance()->debug("End wordpressIDXRequestor.remoteRequest: " );
				
			return $contentInfo ;
		}

		public static function remotePostRequest( $wpidxUrl, $postData ){
			wordpressIDXLogger::getInstance()->debug("Begin wordpressIDXRequestor.remoteRequest: " );

			wordpressIDXLogger::getInstance()->debug("wpidxUrl: " . $wpidxUrl);
			$requestArgs = array('timeout'=>'20', 'body'=>$postData );
			$response = wp_remote_post($wpidxUrl, $requestArgs);
				
			wordpressIDXLogger::getInstance()->debug("wordpressIDXRequestor.remoteRequest post data " );
			wordpressIDXLogger::getInstance()->debugDumpVar($postData);
			wordpressIDXLogger::getInstance()->debugDumpVar($response);

			if( is_wp_error($response)){
				$contentInfo=null;
			}
			else{
				$responseBody = wp_remote_retrieve_body( $response );
				$contentInfo=json_decode($responseBody);
				wordpressIDXLogger::getInstance()->debugDumpVar($responseBody);
			}
				
			wordpressIDXLogger::getInstance()->debug("wordpressIDXRequestor.remoteRequest response " );
			wordpressIDXLogger::getInstance()->debug("End wordpressIDXRequestor.remoteRequest: " );
				
			return $contentInfo ;
		}

		public static function appendQueryVarIfNotEmpty( $wpidxUrl, $queryVarName, $queryVarValue){
			if(isset($queryVarValue, $queryVarName )){
				$queryVarValue=urlencode($queryVarValue);
				$trimmedValue=trim($queryVarValue);
				if( '' != $trimmedValue ){
					if( strpos( $wpidxUrl, "?")){
						$wpidxUrl = $wpidxUrl . "&" . $queryVarName . "=" . $trimmedValue ;
					} else {
						$wpidxUrl = $wpidxUrl . "?" . $queryVarName . "=" . $trimmedValue ;
					}
				}

			}
			return $wpidxUrl ;
		}

		public static function isError($contentInfo){
			$result=false;
			if(is_null($contentInfo) || property_exists($contentInfo, "error")){
				$result=true;
			}
			return $result ;
		}

		/**
		 *
		 * Extract the content from the response.
		 * @param $contentInfo
		 */
		public static function getContent($contentInfo){
			$content='';
			if(is_null($contentInfo)){
				//We could reach this code, if the WordpressIDX services are down.
				$content = "<br/>Sorry we are experiencing system issues.  Please try again.<br/>";
			}
			else if (property_exists($contentInfo, "error")){
				//Report the error from WordpressIDX
				$content = "<br/>" . $contentInfo->error . "</br/>";
			}
			else if( property_exists($contentInfo, "view")){
				//success, display the view
				$content = $contentInfo->view ;
			}
			return $content ;
		}
		
		
		
		public static function addVarsToUrl($url, $arrayOfVars){
			foreach($arrayOfVars as $key=>$val) {
				$paramValue=null;
				if( is_array($val)){
					foreach( $val as $value ){
						if( $paramValue != null ){
							$paramValue .=  ",";
						}
						$paramValue .=  $value;
					}
				} else {
					$paramValue=$val;
				}
				$url = WordpressIDXRequestor::appendQueryVarIfNotEmpty($url, $key, $paramValue );
			}
				
			return $url ;
		}
	}
}
?>