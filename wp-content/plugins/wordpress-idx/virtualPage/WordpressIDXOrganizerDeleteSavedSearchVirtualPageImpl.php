<?php
if( !class_exists('wordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl')) {
	
	class wordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-delete-saved-search-submit";
		public function __construct(){
			
		}

		public function getTitle(){
			return "Delete Saved Search";
		}
			
		public function getPageTemplate(){
			
		}

		public function getPath(){
			return $this->path ;	
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl');
			
			$subscriberId=wordpressIDXUtility::getInstance()->getQueryVar('subscriberID');
			$searchProfileID=wordpressIDXUtility::getInstance()->getQueryVar('searchProfileID');						
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-delete-saved-search-submit' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberID", $subscriberId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "searchProfileID", $searchProfileID);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
						
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			//$content = wordpressIDXRequestor::getContent( $contentInfo );
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl');
			
			$redirectUrl=wordpressIDXUrlFactory::getInstance()->getOrganizerViewSavedSearchListUrl(true) ; 
			//redirect to the list of saved searches to avoid double posting the request
			$content = '<meta http-equiv="refresh" content="0;url=' . $redirectUrl . '">';
			
			return $content ;
		}
	}//end class
}
?>