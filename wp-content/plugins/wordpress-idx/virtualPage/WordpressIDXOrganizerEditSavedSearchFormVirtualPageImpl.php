<?php
if( !class_exists('wordpressIDXOrganizerEditSavedSearchFormVirtualPageImpl')) {
	
	class wordpressIDXOrganizerEditSavedSearchFormVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="email-alerts";
		private $title="Email Alerts";
		
		public function __construct(){
			
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_EMAIL_UPDATES);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_EMAIL_UPDATES);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_EMAIL_UPDATES );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
			
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerViewSavedSearchListVirtualPageImpl');
			
			$subscriberId=wordpressIDXUtility::getInstance()->getQueryVar('subscriberID');
			$searchProfileID=wordpressIDXUtility::getInstance()->getQueryVar('searchProfileID');
			$agentID=wordpressIDXUtility::getInstance()->getQueryVar('agentID');
						
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-edit-saved-search-form' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "searchProfileID", $searchProfileID);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "agentID", $agentID);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true");

			$searchQueryArray=wordpressIDXStateManager::getInstance()->getLastSearchQueryArray();
			
			if( count($searchQueryArray) > 0 ){
				$cityID = trim(wordpressIDXUtility::getInstance()->getVarFromArray("cityID", $searchQueryArray ));
				$zip = trim(wordpressIDXUtility::getInstance()->getVarFromArray("zip", $searchQueryArray ));
				$bedrooms = trim(wordpressIDXUtility::getInstance()->getVarFromArray("bedrooms", $searchQueryArray ));
				$bathCount = trim(wordpressIDXUtility::getInstance()->getVarFromArray("bathCount", $searchQueryArray ) );
				$minListPrice = trim(wordpressIDXUtility::getInstance()->getVarFromArray("minListPrice", $searchQueryArray ) );
				$maxListPrice = trim(wordpressIDXUtility::getInstance()->getVarFromArray("maxListPrice", $searchQueryArray ) );
				$squareFeet = trim($searchQueryArray["squareFeet"]);
				
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "cityID", $cityID);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "zip", $zip);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "bedrooms", $bedrooms);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "bathcount", $bathCount);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "minListPrice", $minListPrice);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "maxListPrice", $maxListPrice);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "squareFeet", $squareFeet);
			}
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;

			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerEditSavedSearchFormVirtualPageImpl');
			
			return $content ;
		}		
	}
}
?>