<?php
if( !class_exists('wordpressIDXOrganizerViewSavedSearchListVirtualPageImpl')) {
	
	class wordpressIDXOrganizerViewSavedSearchListVirtualPageImpl implements wordpressIDXVirtualPage {
		private $path="property-organizer-view-saved-search-list";
		//private $organizerSavedSearchesPath="property-organizer-saved-searches";
		public function __construct(){
			
		}
		public function getTitle(){
			return "Saved Search List";
		}			
			
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerViewSavedSearchListFilterImpl');
			
			$isLoggedIn = wordpressIDXStateManager::getInstance()->isLoggedIn();
			if($isLoggedIn){
				$subscriberInfo=wordpressIDXStateManager::getInstance()->getCurrentSubscriber();
				$subscriberId=$subscriberInfo->getId();
			}	
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-view-saved-search-list' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true");
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerViewSavedSearchListFilterImpl');
			
			return $content ;
		}
	}
}
?>