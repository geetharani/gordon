<?php
if( !class_exists('wordpressIDXFeaturedSearchVirtualPageImpl')) {
	
	class wordpressIDXFeaturedSearchVirtualPageImpl implements wordpressIDXVirtualPage{
	
		private $path ="homes-for-sale-featured";
		private $title="Featured Properties";
		
		public function __construct(){
			
		}
			
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_FEATURED);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_FEATURED);
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_FEATURED );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXFeaturedSearchVirtualPageImpl');
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
						
			$startRowNumber=wordpressIDXUtility::getInstance()->getQueryVar('startRowNumber');
			$sortBy=wordpressIDXUtility::getInstance()->getQueryVar('sortBy');
			$includeMap = wordpressIDXUtility::getInstance()->getRequestVar('includeMap');
			$gallery = wordpressIDXUtility::getInstance()->getRequestVar('gallery');
			
			if( !is_numeric($startRowNumber)){
				$startRowNumber=1;
			}
			
			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=featured-search' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "startRowNumber", $startRowNumber);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "gallery", $gallery);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeMap", $includeMap);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);			
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "sortBy", $sortBy);
			//used to remember search results
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeSearchSummary", "true");				
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);			
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			$content=$idxContent;
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXFeaturedSearchVirtualPageImpl');
			return $content ;
		}
	}
}
?>