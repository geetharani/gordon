<?php
if( !class_exists('wordpressIDXQuickSearchFormVirtualPageImpl')) {
	
	class wordpressIDXQuickSearchFormVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="";
		
		public function __construct(){
			
		}
		public function getTitle(){
			return "";
		}			
			
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXQuickSearchFormFilterImpl.filter');
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=listing-quick-search-form'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true'
				. '&includeJQuery=false';

				
			wordpressIDXLogger::getInstance()->debug('wpidxUrl: ' . $wpidxUrl);	

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXQuickSearchFormFilterImpl.filter');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
}
?>