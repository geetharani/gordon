<?php
if( !class_exists('wordpressIDXOrganizerLoginFormVirtualPageImpl')) {
	
	class wordpressIDXOrganizerLoginFormVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-login";
		private $title="Organizer Login";
		
		public function __construct(){
			
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_ORG_LOGIN);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_ORG_LOGIN);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ORG_LOGIN );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin PropertyOrganizerLoginFormVirtualPage');
			
			$subscriberId=wordpressIDXUtility::getInstance()->getQueryVar('subscriberID');
			if($subscriberId != null && trim($subscriberId) != ""){
				$subscriberInfo=wordpressIDXSubscriber::getInstance($subscriberId,'', '' );
				//var_dump($subscriberInfo);
				wordpressIDXStateManager::getInstance()->saveSubscriberLogin($subscriberInfo);			
			}

			$message=wordpressIDXUtility::getInstance()->getQueryVar('message');
			$afterLoginUrl=wordpressIDXUtility::getInstance()->getRequestVar('afterLoginUrl');		
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-login-form' ;
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "message", $message);
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "afterLoginUrl", $afterLoginUrl);
			
			$isLoggedIn = wordpressIDXStateManager::getInstance()->isLoggedIn();
			if( $isLoggedIn ){
				$subscriberInfo=wordpressIDXStateManager::getInstance()->getCurrentSubscriber() ;
				$subscriberId=$subscriberInfo->getId();
				$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);
			}
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End PropertyOrganizerLoginFormVirtualPage');

				
			return $content ;
		}		
	}
}
?>