<?php
if( !class_exists('wordpressIDXAdvancedSearchFormVirtualPageImpl')) {
	
	class wordpressIDXAdvancedSearchFormVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="homes-for-sale-search-advanced";
		private $title="Advanced Property Search";
		
		public function __construct(){
			
		}
			public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_ADV_SEARCH);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_ADV_SEARCH);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ADV_SEARCH );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAdvancedSearchFormVirtualPageImpl');
			$boardId=wordpressIDXUtility::getInstance()->getQueryVar('bid');
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=listing-advanced-search-form'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true';
				
			if( is_numeric($boardId)){
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardId", $boardId);		
			}

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAdvancedSearchFormVirtualPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
}
?>