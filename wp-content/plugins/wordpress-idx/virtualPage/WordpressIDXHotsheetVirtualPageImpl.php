<?php
if( !class_exists('wordpressIDXHotsheetVirtualPageImpl')) {
	
	class wordpressIDXHotsheetVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="homes-for-sale-toppicks";
		private $title="";
		//The default title might get updated in function getContent
		private $defaultTitle="";
		
		public function __construct(){
			
		}

		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_HOTSHEET);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			else{
				$this->title = $this->defaultTitle ;
			}

			return $this->title ;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_HOTSHEET);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_HOTSHEET);	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXHotsheetVirtualPageImpl');
			
			$includeMap = wordpressIDXUtility::getInstance()->getRequestVar('includeMap');
			$optOut = wordpressIDXUtility::getInstance()->getRequestVar('optout');
			$gallery = wordpressIDXUtility::getInstance()->getRequestVar('gallery');	
			$hotSheetId=wordpressIDXUtility::getInstance()->getQueryVar('hotSheetId');		
			if( !isset($hotSheetId) ){
				//wordpressIDXShortCodeDispatcher sets vars in $_REQUEST
				$hotSheetId=wordpressIDXUtility::getInstance()->getRequestVar('hotSheetId');
			}
			$startRowNumber=wordpressIDXUtility::getInstance()->getQueryVar('startRowNumber');
			$sortBy=wordpressIDXUtility::getInstance()->getQueryVar('sortBy');
						
			wordpressIDXLogger::getInstance()->debug('hotSheetId: ' . $hotSheetId );
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=hotsheet-results'
				. '&authenticationToken=' . $authenticationToken;
				
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeMap", $includeMap);	
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "gallery", $gallery);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "hotSheetId", $hotSheetId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "startRowNumber", $startRowNumber);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "sortBy", $sortBy);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "optOut", $optOut);
			
			if( $this->getTitle() == ""){
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeDisplayName", "false");
			}
			//used to remember search results
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeSearchSummary", "true");	
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			if( isset($contentInfo) && isset($contentInfo->title) ){
				//success, display the view
				$this->defaultTitle = $contentInfo->title ;
			}
						
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXHotsheetVirtualPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
}
?>