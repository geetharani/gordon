<?php
if( !class_exists('WordpressIDXOrganizerSendSubscriberPasswordVirtualPageImpl')) {
	
	class WordpressIDXOrganizerSendSubscriberPasswordVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-send-login";
		
		public function __construct(){
			
		}
		
		public function getTitle(){
			return "Email Password";
		}			
		
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin WordpressIDXOrganizerSendSubscriberPasswordFilterImpl');
			
			$email=wordpressIDXUtility::getInstance()->getQueryVar('email');
						
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-password-email' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End WordpressIDXOrganizerSendSubscriberPasswordFilterImpl');
			
			return $content ;
		}
	}//end class
}
?>