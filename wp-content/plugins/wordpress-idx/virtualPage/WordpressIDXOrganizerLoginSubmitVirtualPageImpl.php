<?php
if( !class_exists('wordpressIDXOrganizerLoginSubmitVirtualPageImpl')) {

	class wordpressIDXOrganizerLoginSubmitVirtualPageImpl implements wordpressIDXVirtualPage {
		private $path="property-organizer-login-submit";
		
		public function __construct(){

		}
		public function getTitle(){
			return "Organizer Login";
		}
					
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;	
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin PropertyOrganizerLoginSubmitVirtualPage');
			
			$subscriberId=wordpressIDXUtility::getInstance()->getQueryVar('subscriberID');

			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-login-submit' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);

			if( $subscriberId == null || trim($subscriberId) == ""){
				//If no subscriber id, then get the authentication info from the request and pass it along
				$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			} else {
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);
			}

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$content = wordpressIDXRequestor::getContent( $contentInfo );
			
			$isLoggedIn = wordpressIDXStateManager::getInstance()->isLoggedIn();
			if( $isLoggedIn && $content == ""){
				$redirectUrl=wordpressIDXUrlFactory::getInstance()->getOrganizerViewSavedListingListUrl();
				//$content = '<meta http-equiv="refresh" content="0;url=' . $redirectUrl . '">';
			}

			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End PropertyOrganizerLoginSubmitVirtualPage');

			return $content ;
		}
	}//end class
}
?>