<?php
if( !class_exists('WordpressIDXOrganizerViewSavedSearchVirtualPageImpl')) {

	class wordpressIDXOrganizerViewSavedSearchVirtualPageImpl implements wordpressIDXVirtualPage {

		private $path="property-organizer-view-saved-search";
		public function __construct(){
				
		}
		public function getTitle(){
			return "Saved Search";
		}

		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;
		}
	
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin WordpressIDXOrganizerViewSavedSearchFilterImpl');

			$searchProfileId=wordpressIDXUtility::getInstance()->getQueryVar('searchProfileID');
			$startRowNumber=wordpressIDXUtility::getInstance()->getQueryVar('startRowNumber');
				
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-view-saved-search' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "searchProfileId", $searchProfileId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "startRowNumber", $startRowNumber);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$content = wordpressIDXRequestor::getContent( $contentInfo );
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End WordpressIDXOrganizerViewSavedSearchFilterImpl');
				
			return $content ;
		}
	}//end class
}
?>