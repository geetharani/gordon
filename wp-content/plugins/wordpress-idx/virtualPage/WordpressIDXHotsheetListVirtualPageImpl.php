<?php
if( !class_exists('wordpressIDXHotsheetListVirtualPageImpl')) {
	
	class wordpressIDXHotsheetListVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="homes-for-sale-toppicks";
		
		public function __construct(){
			
		}
		public function getTitle(){
			return "Top Picks";
		}
				
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return  $this->path;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXHotsheetListVirtualPageImpl');
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=hotsheet-list'
				. '&authenticationToken=' . $authenticationToken;
											
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXHotsheetListVirtualPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
}
?>