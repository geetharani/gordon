<?php
if( !class_exists('wordpressIDXOrganizerResendConfirmationVirtualPageImpl')) {
	
	class wordpressIDXOrganizerResendConfirmationVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path ="property-organizer-resend-confirmation-email";
		
		public function __construct(){
			
		}
		
		public function getTitle(){
			return "Resend Confirmation Email";
		}	
		
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;	
		}	
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerResendConfirmationFilterImpl');
			
			$email=wordpressIDXUtility::getInstance()->getQueryVar('email');
			$password=wordpressIDXUtility::getInstance()->getQueryVar('password');
			$name=wordpressIDXUtility::getInstance()->getQueryVar('name');
			$phone=wordpressIDXUtility::getInstance()->getQueryVar('phone');
			$agentId=wordpressIDXUtility::getInstance()->getQueryVar('agentId');
			$afterLoginUrl=wordpressIDXUtility::getInstance()->getRequestVar('afterLoginUrl');
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-resend-confirm-email' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "afterLoginUrl", $afterLoginUrl);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $name);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone", $phone);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "agentId", agentId);
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerResendConfirmationFilterImpl');
			
			return $content ;
		}
	}//end class
}
?>