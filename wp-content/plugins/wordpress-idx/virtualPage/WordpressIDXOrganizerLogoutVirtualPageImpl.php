<?php
if( !class_exists('WordpressIDXOrganizerLogoutVirtualPageImpl')) {
	
	class wordpressIDXOrganizerLogoutVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-logout";
		
		public function __construct(){
			
		}
		public function getTitle(){
			return "Organizer Logout";
		}
			
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;
		}
		
							
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin WordpressIDXOrganizerLogoutFilterImpl');
			wordpressIDXStateManager::getInstance()->deleteSubscriberLogin();
			wordpressIDXLogger::getInstance()->debug('End WordpressIDXOrganizerLogoutFilterImpl');		
			$redirectUrl=wordpressIDXUrlFactory::getInstance()->getListingsSearchFormUrl(true) ; 
			//redirect to the search page
			$content = '<meta http-equiv="refresh" content="0;url=' . $redirectUrl . '">';
			
			return $content ;
		}
	}//end class
}
?>