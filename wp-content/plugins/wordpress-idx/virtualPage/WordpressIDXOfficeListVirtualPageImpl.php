<?php
if( !class_exists('wordpressIDXOfficeListVirtualPageImpl')) {
	
	class wordpressIDXOfficeListVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="office-list";
		private $title="Office List";
	
		public function __construct(){
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_OFFICE_LIST);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_LIST);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_LIST );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOfficeListPageImpl');

			//used to remember search results
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=office-list'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true'
				. '&includeSearchSummary=true';


			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOfficeListPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
		
}
?>