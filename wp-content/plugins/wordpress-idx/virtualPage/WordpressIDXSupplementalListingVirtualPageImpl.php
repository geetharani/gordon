<?php
if( !class_exists('wordpressIDXSupplementalListingVirtualPageImpl')) {
	
	class wordpressIDXSupplementalListingVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="supplemental-listing";
		private $title="Supplemental Listings";
	
		public function __construct(){
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_SUPPLEMENTAL_LISTING);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_SUPPLEMENTAL_LISTING);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SUPPLEMENTAL_LISTING );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXSupplementalListingPageImpl');
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
			//used to remember search results
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=supplemental-listing'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true'
				. '&includeSearchSummary=true';


			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXSupplementalListingPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
		
}
?>