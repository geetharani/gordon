<?php
if( !class_exists('wordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl')) {
	
	class wordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="email-updates-confirmation";
		public function __construct(){
			
		}
		public function getTitle(){
			return "Email Updates Confirmation";
		}	

		public function getPageTemplate(){
			
		}

		public function getPath(){
			return $this->path;
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl');
			$message=wordpressIDXUtility::getInstance()->getQueryVar('message');		
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-email-updates-confirmation' ;
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "message", $message);
						
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );

			$content=$idxContent;
				
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl');
			
			return $content ;
		}		
	}
}
?>
