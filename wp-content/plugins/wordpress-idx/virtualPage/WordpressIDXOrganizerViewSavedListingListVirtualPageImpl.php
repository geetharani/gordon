<?php
if( !class_exists('wordpressIDXOrganizerViewSavedListingListVirtualPageImpl')) {
	
	class wordpressIDXOrganizerViewSavedListingListVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-saved-listings";	
		public function __construct(){
			
		}
		public function getTitle(){
			return "Saved Listing List";
		}	

	
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerViewSavedListingListFilterImpl');
			
			$isLoggedIn = wordpressIDXStateManager::getInstance()->isLoggedIn();
			if($isLoggedIn){
				$subscriberInfo=wordpressIDXStateManager::getInstance()->getCurrentSubscriber();
				$subscriberId=$subscriberInfo->getId();
			}
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-view-saved-listing-list' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true");
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerViewSavedListingListFilterImpl');
			
			return $content ;
		}
	}
}
?>