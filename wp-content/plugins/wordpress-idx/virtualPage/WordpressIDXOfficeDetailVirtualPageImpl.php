<?php
if( !class_exists('wordpressIDXOfficeDetailVirtualPageImpl')) {
	
	class wordpressIDXOfficeDetailVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="office-detail";
		private $title="";
		private $defaultTitle="Office Detail";
	
		public function __construct(){
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_OFFICE_DETAIL);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			else{
				$this->title = $this->defaultTitle ;
			}

			return $this->title ;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_DETAIL);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_DETAIL );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOfficeDetailPageImpl');

			$officeID=wordpressIDXUtility::getInstance()->getQueryVar('officeID');
			//used to remember search results
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=office-detail'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true';

			if( is_numeric($officeID)){
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "officeID", $officeID);		
			}
				
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			if( property_exists($contentInfo, "title")){
				//success, display the view
				$this->defaultTitle = $contentInfo->title ;
			}					
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOfficeDetailPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
		
}
?>