<?php
if( !interface_exists('wordpressIDXVirtualPage')) {
	interface wordpressIDXVirtualPage {
		function getContent( $authenticationToken ) ;
		function getTitle();
		function getPageTemplate();
		function getPath();
	}
}//end if( !class_exists('wordpressIDXFilter')) 
?>