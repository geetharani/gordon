<?php
if( !class_exists('wordpressIDXAgentListVirtualPageImpl')) {
	
	class wordpressIDXAgentListVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="agent-list";
		private $title="Agent List";
	
		public function __construct(){
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_AGENT_LIST);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}			
			return $this->title;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_LIST);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_LIST );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAgentListPageImpl');

			//used to remember search results
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=agent-list'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true'
				. '&includeSearchSummary=true';


			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAgentListPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
		
}
?>