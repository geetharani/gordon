<?php
if( !class_exists('wordpressIDXOrganizerEditSavedSearchVirtualPageImpl')) {
	
	class wordpressIDXOrganizerEditSavedSearchVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path ="property-organizer-edit-saved-search-submit";
		public function __construct(){
			
		}
		public function getTitle(){
			return "Email Alert";
		}		
				
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path;
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerEditSavedSearchVirtualPageImpl');
						
			$searchProfileName=wordpressIDXUtility::getInstance()->getQueryVar('searchProfileName');
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-edit-saved-search-submit' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $searchProfileName);
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			//$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true");
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$content = wordpressIDXRequestor::getContent( $contentInfo );

//			if(wordpressIDXStateManager::getInstance()->isLoggedIn()){	
//				$redirectUrl=wordpressIDXUrlFactory::getInstance()->getOrganizerViewSavedSearchListUrl(true) ; 
//				//redirect to the list of saved searches to avoid double posting the request
//				$content = '<meta http-equiv="refresh" content="0;url=' . $redirectUrl . '">';
//			} else {
//				$redirectUrl=wordpressIDXUrlFactory::getInstance()->getOrganizerEmailUpdatesConfirmationUrl(true) ; 
//				//redirect to the list of saved searches to avoid double posting the request
//				$content = '<meta http-equiv="refresh" content="0;url=' . $redirectUrl . '">';
//			}
				
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerEditSavedSearchVirtualPageImpl');
			
			return $content ;
		}
	}//end class
}
?>