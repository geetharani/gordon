<?php
if( !class_exists('wordpressIDXOrganizerEditSubscriberVirtualPageImpl')) {
	
	class wordpressIDXOrganizerEditSubscriberVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-edit-subscriber";	
		public function __construct(){
			
		}
		public function getTitle(){
			return "Organizer Help";
		}	
	
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerEditSubscriberVirtualPageImpl');
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-edit-subscriber' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true");
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerEditSubscriberVirtualPageImpl');
			
			return $content ;
		}
	}
}
?>