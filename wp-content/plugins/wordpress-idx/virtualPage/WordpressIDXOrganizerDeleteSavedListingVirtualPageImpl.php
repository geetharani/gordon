<?php
if( !class_exists('wordpressIDXOrganizerDeleteSavedListingVirtualPageImpl')) {
	
	class wordpressIDXOrganizerDeleteSavedListingVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="property-organizer-delete-saved-listing-submit";
		public function __construct(){
			
		}

		public function getTitle(){
			return "Saved Listing List";
		}
			
		public function getPageTemplate(){
			
		}

		public function getPath(){
			return $this->path ;	
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXOrganizerDeleteSavedListingVirtualPageImpl');

			$savedListingId=wordpressIDXUtility::getInstance()->getQueryVar('savedListingID');		
	
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-delete-saved-listing-submit' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "savedListingId", $savedListingId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			//wordpressIDXRequestor will append the subscriber id to this request.			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$content = wordpressIDXRequestor::getContent( $contentInfo );
						
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXOrganizerDeleteSavedListingVirtualPageImpl');
			
			
			return $content ;
		}
	}//end class
}
?>