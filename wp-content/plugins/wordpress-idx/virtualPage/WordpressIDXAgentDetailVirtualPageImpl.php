<?php
if( !class_exists('wordpressIDXAgentDetailVirtualPageImpl')) {
	
	class wordpressIDXAgentDetailVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path="agent-detail";
		private $title="";
		private $defaultTitle="Agent Bio";
	
		public function __construct(){
		}
		
		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_AGENT_DETAIL);
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			else{
				$this->title = $this->defaultTitle ;
			}

			return $this->title ;
		}
	
		public function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_DETAIL);
			//$pageTemplage = '';
			return $pageTemplate;			
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_DETAIL );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAgentDetailPageImpl');
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
			
			$agentID=wordpressIDXUtility::getInstance()->getQueryVar('agentID');
			//used to remember search results
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=agent-detail'
				. '&authenticationToken=' . $authenticationToken
				. '&phpStyle=true'
				. '&includeSearchSummary=true';

			if( is_numeric($agentID)){
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "agentID", $agentID);		
			}

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			
			$content=$idxContent;
			
			if( property_exists($contentInfo, "title")){
				//success, display the view
				$this->defaultTitle = $contentInfo->title ;
			}			
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAgentDetailPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
		
}
?>