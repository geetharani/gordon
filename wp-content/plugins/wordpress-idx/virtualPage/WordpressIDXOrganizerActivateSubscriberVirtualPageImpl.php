<?php
if( !class_exists('WordpressIDXOrganizerActivateSubscriberVirtualPageImpl')) {
	
	class wordpressIDXOrganizerActivateSubscriberVirtualPageImpl implements wordpressIDXVirtualPage {
	
		private $path= "property-organizer-activate";
		
		public function __construct(){
			
		}
		public function getTitle(){
			return "Subscriber Activation";
		}		
				
		public function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;			
		}
		
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin WordpressIDXOrganizerActivateSubscriberVirtualPageImpl');
			
			$email=wordpressIDXUtility::getInstance()->getQueryVar('email');
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=property-organizer-activate-subscriber' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End WordpressIDXOrganizerActivateSubscriberVirtualPageImpl');
			
			return $content ;
		}
	}//end class
}
?>