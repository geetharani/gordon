<?php
if( !class_exists('wordpressIDXSearchResultsVirtualPageImpl')) {
	
	class wordpressIDXSearchResultsVirtualPageImpl implements wordpressIDXVirtualPage {
		
		//default path used for URL Rewriting
		private $path="homes-for-sale-results";
	
		public function __construct(){
			
		}
		
		public function getTitle(){
			return "Property Search Results";
		}	
			
		function getPageTemplate(){
			
		}
		
		public function getPath(){
			return $this->path ;
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXFilter.filterSearchResults');
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
			
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=listing-search-results' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			//used to remember search results
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "includeSearchSummary", "true");	
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
	
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXFilter.filterSearchResults');
						
			return $content ;
		}		
	}
}
?>