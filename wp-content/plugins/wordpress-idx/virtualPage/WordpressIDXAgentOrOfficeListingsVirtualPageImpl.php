<?php
if( !class_exists('wordpressIDXAgentOrOfficeListingsVirtualPageImpl')) {
	
	/**
	 * 
	 * This virtual page is used in a shortcode and does not have a title, template or path.
	 * 
	 * @author wordpressIDX
	 *
	 */
	class wordpressIDXAgentOrOfficeListingsVirtualPageImpl implements wordpressIDXVirtualPage {
		
		public function __construct(){			
		}

		
		/**
		 * @see wp-content/plugins/wordpressIDX/virtualPage/wordpressIDXVirtualPage::getTitle()
		 */
		public function getTitle(){
			return "" ;
		}
	

		/**
		 * @see wp-content/plugins/wordpressIDX/virtualPage/wordpressIDXVirtualPage::getPageTemplate()
		 */
		public function getPageTemplate(){
			return "";			
		}
		
		/**
		 * @see wp-content/plugins/wordpressIDX/virtualPage/wordpressIDXVirtualPage::getPath()
		 */
		public function getPath(){
			return "";
		}
				
		public function getContent( $authenticationToken ){
			wordpressIDXStateManager::getInstance()->saveLastSearch() ;
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAgentOrOfficeListingsVirtualPageImpl');
			
			$agentId  = wordpressIDXUtility::getInstance()->getRequestVar('agentId');
			$officeId = wordpressIDXUtility::getInstance()->getRequestVar('officeId');	

			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL 
				. '?method=handleRequest'
				. '&viewType=json'
				. '&requestType=agent-or-office-listings'
				. '&authenticationToken=' . $authenticationToken;
				
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "agentId", $agentId);	
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "officeId", $officeId);
						
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
						
			$content=$idxContent;
			
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAgentOrOfficeListingsVirtualPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			return $content ;
		}
	}
}
?>