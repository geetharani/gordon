<?php
if( !class_exists('wordpressIDXListingDetailVirtualPageImpl')) {

	class wordpressIDXListingDetailVirtualPageImpl implements wordpressIDXVirtualPage {

		private $defaultTitle="";
		private $title = "";
		private $pageTitle=null;
		private $path ="homes-for-sale-details";
		public function __construct(){

		}

		public function getTitle(){
			$customTitle = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_DETAIL);				
			if( $customTitle != null && "" != $customTitle ){
				$this->title=$customTitle ;
			}
			else{
				$this->title = $this->defaultTitle ;
			}

			return $this->title ;
		}
		
		public function getPath(){
			$customPath = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_DETAIL );	
			if( $customPath != null && "" != $customPath ){
				$this->path = $customPath ;
			}
			return $this->path;
		}
		
		function getPageTemplate(){
			$pageTemplate = get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_DETAIL);
			//$pageTemplate = get_theme_root() . '/twentyeleven/sidebar-page.php';
			//$pageTemplage = '';
			return $pageTemplate;
		}

		public function getContent( $authenticationToken ){
			global $post;
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXListingDetailVirtualPageImpl');

			$listingNumber=wordpressIDXUtility::getInstance()->getQueryVar('ln');
			$boardId=wordpressIDXUtility::getInstance()->getQueryVar('bid');
			$wpidxUrl = wordpressIDXConstants::EXTERNAL_URL
				. '?ln=' . $listingNumber
				. '&bid=' . $boardId
				. '&method=handleRequest'
				. '&viewType=json'
				. '&requestType=listing-detail'
				. '&authenticationToken=' . $authenticationToken;

			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			
			$wpidxUrl = wordpressIDXUtility::getInstance()->setPreviousAndNextInformation($wpidxUrl,$boardId, $listingNumber ) ;

			wordpressIDXLogger::getInstance()->debug('before logged in check');
			if( wordpressIDXStateManager::getInstance()->isLoggedIn() ){
				wordpressIDXLogger::getInstance()->debug('is logged in');
				$subscriberInfo=wordpressIDXStateManager::getInstance()->getCurrentSubscriber() ;
				$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberInfo->getId());
			}

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl);
			$idxContent = wordpressIDXRequestor::getContent( $contentInfo );
			$content=$idxContent;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXListingDetailVirtualPageImpl');
			wordpressIDXLogger::getInstance()->debug('<br/><br/>' . $wpidxUrl);
			
			if( property_exists($contentInfo, "title")){
				//success, display the view
				$this->defaultTitle = $contentInfo->title ;
			}

			$previousSearchLink = $this->getPreviousSearchLink();
			$content = $previousSearchLink . '<br/><br/>' . $content ;

			return $content ;
		}
		


		/**
		 *
		 * @param unknown_type $content
		 */
		private function getPreviousSearchLink(){

			$previousSearchUrl=wordpressIDXStateManager::getInstance()->getLastSearch();

			//If previous search does not exist, then use an empty search form
			if( $previousSearchUrl == null || trim( $previousSearchUrl) == ''){
				$previousSearchUrl= wordpressIDXUrlFactory::getInstance()->getListingsSearchFormUrl(true);
				$previousSearchUrl="<a href='" . $previousSearchUrl . "'>&lt;&nbsp;New Search</a>";
			}
			else{
				$previousSearchUrl="<a href='" . $previousSearchUrl . "'>&lt;&nbsp;Return To Results</a>";
			}

			return $previousSearchUrl;
		}
	}
}
?>