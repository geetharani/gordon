<?php
if( !class_exists('wordpressIDXVirtualPageFactory')) {
	include_once(   'virtualPage/WordpressIDXVirtualPage.php');
	include_once(   'virtualPage/WordpressIDXFeaturedSearchVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXHotsheetVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXHotsheetListVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXAdvancedSearchFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXSearchFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXQuickSearchFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXSearchResultsVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXListingDetailVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXListingSoldDetailVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXOrganizerLoginFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerLogoutVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerLoginSubmitVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXOrganizerEditSavedSearchVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerEditSavedSearchFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXOrganizerViewSavedSearchVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerViewSavedSearchListVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXOrganizerViewSavedListingListVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerDeleteSavedListingVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerResendConfirmationVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerActivateSubscriberVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerSendSubscriberPasswordVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerHelpVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOrganizerEditSubscriberVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXContactFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXValuationFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOpenHomeSearchFormVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXSoldFeaturedListingVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXSupplementalListingVirtualPageImpl.php');

	include_once(   'virtualPage/WordpressIDXOfficeListVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXOfficeDetailVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXAgentListVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXAgentDetailVirtualPageImpl.php');
	include_once(   'virtualPage/WordpressIDXAgentOrOfficeListingsVirtualPageImpl.php');

	/**
	 * This singleton class creates instances of WordpressIDX VirtualPages, based
	 * on a type parameter.
	 * @author wordpressIDX
	 */
	class wordpressIDXVirtualPageFactory {

		private static $instance ;

		private function __construct(){
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXVirtualPageFactory();
			}
			return self::$instance;
		}

		////////////////////////////////////////////////////////
		//Types used to determine the VirtualPage type in WordpressIDXVirtualPageFactory.
		const LISTING_SEARCH_RESULTS="idx-results";
		const LISTING_DETAIL="idx-detail";
		const LISTING_SOLD_DETAIL="idx-sold-detail";
		const LISTING_SEARCH_FORM="idx-search";
		const LISTING_QUICK_SEARCH_FORM="idx-quick-search";
		const LISTING_ADVANCED_SEARCH_FORM="idx-advanced-search";
		const FEATURED_SEARCH="idx-featured-search";
		const HOTSHEET_SEARCH_RESULTS="idx-toppicks";
		const HOTSHEET_LIST="idx-toppicks-list";
		const ORGANIZER_LOGIN="idx-property-organizer-login";
		const ORGANIZER_LOGOUT="idx-property-organizer-logout";
		const ORGANIZER_LOGIN_SUBMIT="idx-property-organizer-submit-login";
		const ORGANIZER_EDIT_SAVED_SEARCH="idx-property-organizer-edit-saved-search";
		const ORGANIZER_EDIT_SAVED_SEARCH_SUBMIT="idx-property-organizer-edit-saved-search-submit";
		const ORGANIZER_EMAIL_UPDATES_CONFIRMATION="idx-property-organizer-email-updates-success";
		const ORGANIZER_DELETE_SAVED_SEARCH="idx-property-organizer-delete-saved-search";
		const ORGANIZER_DELETE_SAVED_SEARCH_SUBMIT="idx-property-organizer-delete-saved-search-submit";
		const ORGANIZER_VIEW_SAVED_SEARCH="idx-property-organizer-view-saved-search";
		const ORGANIZER_VIEW_SAVED_SEARCH_LIST="idx-property-organizer-view-saved-searches";
		const ORGANIZER_VIEW_SAVED_LISTING_LIST="idx-property-organizer-view-saved-listings";
		const ORGANIZER_DELETE_SAVED_LISTING_SUBMIT="idx-property-organizer-delete-saved-listing";
		const ORGANIZER_RESEND_CONFIRMATION_EMAIL="idx-property-organizer-resend-confirmation-email";
		const ORGANIZER_ACTIVATE_SUBSCRIBER ="idx-property-organizer-activate-subscriber";
		const ORGANIZER_SEND_SUBSCRIBER_PASSWORD="idx-property-organizer-send-login";
		const ORGANIZER_HELP="idx-property-organizer-help";
		const ORGANIZER_EDIT_SUBSCRIBER="idx-property-organizer-edit-subscriber";

		const CONTACT_FORM="idx-contact-form";
		const VALUATION_FORM="idx-valuation-form";
		const OPEN_HOME_SEARCH_FORM="idx-open-home-search-form";
		const SUPPLEMENTAL_LISTING="idx-supplemental-listing";
		const SOLD_FEATURED_LISTING="idx-sold-featured-listing";
		
		const OFFICE_LIST="idx-office-list";
		const OFFICE_DETAIL="idx-office-detail";
		const AGENT_LIST="idx-agent-list";
		const AGENT_DETAIL="idx-agent-detail";
		const AGENT_OR_OFFICE_LISTINGS="idx-agent-or-office-listings";
		///////////////////////////////////////////////////////

		public function getVirtualPage( $type ){
			$virtualPage ;
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXVirtualPageFactory.getVirtualPage type=' . $type);
			if($type == wordpressIDXVirtualPageFactory::LISTING_SEARCH_RESULTS ){
				$virtualPage = new wordpressIDXSearchResultsVirtualPageImpl();
			}
			else if($type == wordpressIDXVirtualPageFactory::LISTING_DETAIL ){
				$virtualPage = new wordpressIDXListingDetailVirtualPageImpl();
			}
			else if($type == wordpressIDXVirtualPageFactory::LISTING_SOLD_DETAIL ){
				$virtualPage = new wordpressIDXListingSoldDetailVirtualPageImpl();
			}			
			else if( $type == wordpressIDXVirtualPageFactory::FEATURED_SEARCH){
				$virtualPage = new wordpressIDXFeaturedSearchVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::LISTING_ADVANCED_SEARCH_FORM){
				$virtualPage = new wordpressIDXAdvancedSearchFormVirtualPageImpl();
			}
	    	else if( $type == wordpressIDXVirtualPageFactory::LISTING_SEARCH_FORM){
				$virtualPage = new wordpressIDXSearchFormVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::LISTING_QUICK_SEARCH_FORM){
				$virtualPage = new wordpressIDXQuickSearchFormVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::HOTSHEET_SEARCH_RESULTS ){
				$virtualPage = new wordpressIDXHotsheetVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::HOTSHEET_LIST ){
				$virtualPage = new wordpressIDXHotsheetListVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_LOGIN ){
				$virtualPage = new WordpressIDXOrganizerLoginFormVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_LOGOUT ){
				$virtualPage = new WordpressIDXOrganizerLogoutVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_LOGIN_SUBMIT ){
				$virtualPage = new WordpressIDXOrganizerLoginSubmitVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_EDIT_SAVED_SEARCH ){
				$virtualPage = new wordpressIDXOrganizerEditSavedSearchFormVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_EMAIL_UPDATES_CONFIRMATION ){
				$virtualPage = new wordpressIDXOrganizerEmailUpdatesConfirmationVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_EDIT_SAVED_SEARCH_SUBMIT ){
				$virtualPage = new wordpressIDXOrganizerEditSavedSearchVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_DELETE_SAVED_SEARCH_SUBMIT ){
				$virtualPage = new wordpressIDXOrganizerDeleteSavedSearchVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_VIEW_SAVED_SEARCH ){
				$virtualPage = new wordpressIDXOrganizerViewSavedSearchVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_VIEW_SAVED_SEARCH_LIST ){
				$virtualPage = new wordpressIDXOrganizerViewSavedSearchListVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_VIEW_SAVED_LISTING_LIST ){
				$virtualPage = new wordpressIDXOrganizerViewSavedListingListVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_DELETE_SAVED_LISTING_SUBMIT){
				$virtualPage = new WordpressIDXOrganizerDeleteSavedListingVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_ACTIVATE_SUBSCRIBER){
				$virtualPage = new WordpressIDXOrganizerActivateSubscriberVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_RESEND_CONFIRMATION_EMAIL ){
				$virtualPage = new wordpressIDXOrganizerResendConfirmationVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_SEND_SUBSCRIBER_PASSWORD ){
				$virtualPage = new WordpressIDXOrganizerSendSubscriberPasswordVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_HELP ){
				$virtualPage = new WordpressIDXOrganizerHelpVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::ORGANIZER_EDIT_SUBSCRIBER ){
				$virtualPage = new WordpressIDXOrganizerEditSubscriberVirtualPageImpl() ;
			}
			else if( $type == wordpressIDXVirtualPageFactory::CONTACT_FORM ){
				$virtualPage = new wordpressIDXContactFormVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::VALUATION_FORM ){
				$virtualPage = new wordpressIDXValuationFormVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::OPEN_HOME_SEARCH_FORM ){
				$virtualPage = new wordpressIDXOpenHomeSearchFormVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::SUPPLEMENTAL_LISTING ){
				$virtualPage = new wordpressIDXSupplementalListingVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::SOLD_FEATURED_LISTING ){
				$virtualPage = new wordpressIDXSoldFeaturedListingVirtualPageImpl();
			}
			else if( $type == wordpressIDXVirtualPageFactory::OFFICE_LIST ){
				$virtualPage = new wordpressIDXOfficeListVirtualPageImpl();
			}			
			else if( $type == wordpressIDXVirtualPageFactory::OFFICE_DETAIL ){
				$virtualPage = new wordpressIDXOfficeDetailVirtualPageImpl();
			}			
			else if( $type == wordpressIDXVirtualPageFactory::AGENT_LIST ){
				$virtualPage = new wordpressIDXAgentListVirtualPageImpl();
			}			
			else if( $type == wordpressIDXVirtualPageFactory::AGENT_DETAIL ){
				$virtualPage = new wordpressIDXAgentDetailVirtualPageImpl();
			}			
			else if($type == wordpressIDXVirtualPageFactory::AGENT_OR_OFFICE_LISTINGS ){
				$virtualPage = new wordpressIDXAgentOrOfficeListingsVirtualPageImpl() ;
			}

			wordpressIDXLogger::getInstance()->debug('Complete wordpressIDXVirtualPageFactory.getVirtualPage');
			return $virtualPage ;
		}
	}
}
?>