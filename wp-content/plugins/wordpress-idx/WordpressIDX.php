<?php
	/*
	Plugin Name: Wordpress IDX Plugin
	Plugin URI: http://wordpress.org/extend/plugins/wordpress-idx/
	Description: This plugin integrates your Wordpress site with IDX search functionality.  This plugin requires an activation key.
	Version: 1.3.5
	Author: ihomefinder
	Author URI: http://www.ihomefinder.com
	License: GPL
	*/


/**
 * Load core files
 */
include_once 'WordpressIDXAdmin.php';
include_once 'WordpressIDXAjaxHandler.php';
include_once 'WordpressIDXConstants.php';
include_once 'WordpressIDXCustomization.php';
include_once 'WordpressIDXInstaller.php';
include_once 'WordpressIDXLogger.php';
include_once 'WordpressIDXPermissions.php';
include_once 'WordpressIDXRequestor.php';
include_once 'WordpressIDXRewriteRules.php';
include_once 'WordpressIDXShortcodeDispatcher.php';
include_once 'WordpressIDXStateManager.php';
include_once 'WordpressIDXCleaner.php';
include_once 'WordpressIDXSubscriber.php';
include_once 'WordpressIDXTinyMceManager.php';
include_once 'WordpressIDXUrlFactory.php';
include_once 'WordpressIDXUtility.php';
include_once 'WordpressIDXVirtualPageDispatcher.php';
include_once 'WordpressIDXVirtualPageFactory.php';
include_once 'WordpressIDXVirtualPageHelper.php';


/**
 * Load  Widgets and Widget Context Utility
 */
include("widget/WordpressIDXWidgetContextUtility.php");
include("widget/WordpressIDXPropertiesGallery.php");
include("widget/WordpressIDXQuickSearchWidget.php");

add_action('widgets_init', create_function('', 'return register_widget("WordpressIDXPropertiesGallery");'));
add_action('widgets_init', create_function('', 'return register_widget("WordpressIDXQuickSearchWidget");'));

/* Runs when plugin is activated */
register_activation_hook(__FILE__,array(wordpressIDXInstaller::getInstance(), 'install'));
/* Runs on plugin deactivation*/
register_deactivation_hook( __FILE__, array(wordpressIDXInstaller::getInstance(), 'remove') );


/* Runs just before the auto upgrader installs the plugin*/
add_filter('upgrader_post_install', array(wordpressIDXInstaller::getInstance(), 'upgrade'), 10, 2);

/* Rewrite Rules */
add_action('init', array(wordpressIDXRewriteRules::getInstance(), "initialize"), 1 );



//uncomment during development, so rule changes can be viewed.
//in production this should not run, because it is a slow operation.
//add_action('init',array(wordpressIDXRewriteRules::getInstance(), "flushRules"));

if( is_admin()){
	add_action('admin_menu', array(wordpressIDXAdmin::getInstance(), "createAdminMenu"));
	add_action('admin_init', array(wordpressIDXInstaller::getInstance(), 'upgrade'));
	add_action('admin_init', array(wordpressIDXAdmin::getInstance(), "registerSettings") );
		
	add_action('admin_init', array(wordpressIDXWidgetContextUtility::getInstance(), "loadWidgetJavascript") );
	//Adds functionality to the text editor for pages and posts
	//Add buttons to text editor and initialize short codes
	add_action('admin_init', array(wordpressIDXTinyMceManager::getInstance(), "addButtons") );	
} else {
	add_action('init',array(wordpressIDXVirtualPageDispatcher::getInstance(), "loadJavaScript")) ;
	
	//Remember the users state in the application (subscriber info and last search)
	add_action('plugins_loaded',array(wordpressIDXStateManager::getInstance(), "initialize"), 5);

	add_filter( 'page_template', array(wordpressIDXVirtualPageDispatcher::getInstance(), "getPageTemplate") );
	add_filter( 'the_content', array(wordpressIDXVirtualPageDispatcher::getInstance(), "getContent"), 20 );
	add_filter( 'the_posts', array(wordpressIDXVirtualPageDispatcher::getInstance(), "postCleanUp") );
	//add_filter( 'the_title', array(wordpressIDXVirtualPageDispatcher::getInstance(), "getTitle") );
	
	add_action('wp_footer', array(wordpressIDXCustomization::getInstance(), "addCustomCSS"));	
	
	add_action('wpidx_expired_transients_cleanup', array(wordpressIDXCleaner::getInstance(), "removeExpiredwpidxTransients"));
}


add_action('init', array(wordpressIDXShortcodeDispatcher::getInstance(), "init"));

//AJAX Request handling.
add_action("wp_ajax_nopriv_ihf_more_info_request", array(wordpressIDXAjaxHandler::getInstance(), "requestMoreInfo")) ;
add_action("wp_ajax_nopriv_ihf_schedule_showing",  array(wordpressIDXAjaxHandler::getInstance(), "scheduleShowing"));
add_action("wp_ajax_nopriv_ihf_save_property",     array(wordpressIDXAjaxHandler::getInstance(), "saveProperty")) ;
add_action("wp_ajax_nopriv_ihf_photo_tour",        array(wordpressIDXAjaxHandler::getInstance(), "photoTour")) ;
add_action("wp_ajax_nopriv_ihf_save_search",        array(wordpressIDXAjaxHandler::getInstance(), "saveSearch")) ;
add_action("wp_ajax_nopriv_ihf_advanced_search_multi_selects", array(wordpressIDXAjaxHandler::getInstance(), "advancedSearchMultiSelects")) ;
add_action("wp_ajax_nopriv_ihf_advanced_search_fields",        array(wordpressIDXAjaxHandler::getInstance(), "getAdvancedSearchFormFields")) ;
add_action("wp_ajax_nopriv_ihf_lead_capture_login",            array(wordpressIDXAjaxHandler::getInstance(), "leadCaptureLogin")) ;

add_action("wp_ajax_ihf_more_info_request",        array(wordpressIDXAjaxHandler::getInstance(), "requestMoreInfo")) ;
add_action("wp_ajax_ihf_schedule_showing",         array(wordpressIDXAjaxHandler::getInstance(), "scheduleShowing"));
add_action("wp_ajax_ihf_save_property",            array(wordpressIDXAjaxHandler::getInstance(), "saveProperty")) ;
add_action("wp_ajax_ihf_photo_tour",               array(wordpressIDXAjaxHandler::getInstance(), "photoTour")) ;
add_action("wp_ajax_ihf_save_search",              array(wordpressIDXAjaxHandler::getInstance(), "saveSearch")) ;
add_action("wp_ajax_ihf_advanced_search_multi_selects",       array(wordpressIDXAjaxHandler::getInstance(), "advancedSearchMultiSelects")) ;
add_action("wp_ajax_ihf_advanced_search_fields",              array(wordpressIDXAjaxHandler::getInstance(), "getAdvancedSearchFormFields")) ;
add_action("wp_ajax_ihf_lead_capture_login",                  array(wordpressIDXAjaxHandler::getInstance(), "leadCaptureLogin")) ;

//Disable canonical urls, because we use a single page to display all results
//and Wordpress creates a single canonical url for all of the virtual urls
//like the detail page and featured results.
remove_action('wp_head', 'rel_canonical');
//remove_action('wp_head', 'genesis_canonical');
?>