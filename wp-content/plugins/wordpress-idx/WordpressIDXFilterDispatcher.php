<?php
if( !class_exists('wordpressIDXFilterDispatcher')) {

	/**
	 *
	 * This singleton class is used to filter the content of WordpressIDX pages.
	 * We use the WordpressIDXFilterFactory class to retrieve the
	 * proper filter implementation.
	 *
	 * @author wordpressIDX
	 */
	class wordpressIDXFilterDispatcher {

		private static $instance ;
		private $wpidxAdmin ;

		private $currentFilter = null;
		private $content = null;
		private $title = null;
		private $initialized=false;

		private function __construct(){
			$this->wpidxAdmin = wordpressIDXAdmin::getInstance();
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXFilterDispatcher();
			}
			return self::$instance;
		}

		private function init(){
			global $wp_query ;
			
			$postsCount = $wp_query->post_count ;
			//we only try to initialize, if we are accessing a virtual page
			//which does not have any true posts in the global posts array	
			if( !$this->initialized && $postsCount == 0 ){
				if( $type = get_query_var(wordpressIDXConstants::wpidx_TYPE_URL_VAR) ) {
					$this->currentFilter= wordpressIDXFilterFactory::getInstance()->getFilter($type);
					$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();
					$this->content=$this->currentFilter->filter('', $authenticationToken);
					$this->title=$this->currentFilter->getTitle();
					$this->initialized=true;
				}
			}
		}
		
		/**
		 * Cleanup state after filtering.  This fixes an issue
		 * where widgets display different loop content, such
		 * as featured posts.
		 */
		private function afterFilter(){
			$this->initialized=false;
		}

		/**
		 * We identify WordpressIDX requests based on the query_var
		 * WordpressIDXConstants::wpidx_TYPE_URL_VAR.
		 * Set the proper title and update the posts array to contain only
		 * a single posts.  This will get updated in another action later
		 * during processing.  We cannot set the post content here, because
		 * Wordpress does some odd formatting of the post_content, if we
		 * add it here (see the filter method below, where content is properly set)
		 *
		 * @param $posts
		 */
		function postCleanUp($posts){
			$this->init();
			if( $this->initialized ){
				$title = $this->currentFilter->getTitle();
				$_postArray['post_title'] = $title ;
				$_postArray['post_content'] = 'wpidx' ;
				$_postArray['post_excerpt'] = ' ' ;
				$_postArray['post_status'] = 'publish';
				$_postArray['post_type'] = 'page';
				$_postArray['comment_status'] = 'closed';
				$_postArray['ping_status'] = 'closed';
				$_postArray['post_category'] = array(1); // the default 'Uncategorized'
				$_postArray['post_parent'] = 0;
				$_postArray['post_author'] = 0;
				$_postArray['post_date'] = current_time('mysql');
				$_postObject=(object) $_postArray ;
				$_postObject=get_post($_postObject);

				$posts= array();
				$posts[0]=$_postObject;
			}
			return $posts ;
		}

		/**
		 * For the wpidx plugin page, we replace the content, with data retrieved from
		 * the WordpressIDX servers.
		 *
		 * This function uses a Factory to get the correct filter implementation.
		 *
		 * @param $content
		 */
		function filter( $content ) {
			$this->init();
			if( $this->initialized ){
				$content = $this->content;
			}
			//reset init params
			$this->afterFilter() ;
			return $content;
		}

		function filterByType( $content, $type ) {
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXFilterDispatcher.filter');

			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();

			if( $type ) {
				$wpidxFilter = wordpressIDXFilterFactory::getInstance()->getFilter($type);
				$content=$wpidxFilter->filter($content, $authenticationToken);

			}

			wordpressIDXLogger::getInstance()->debug('Complete function wordpressIDXFilterDispatcher.filter');
			return $content;
		}
	}
}//end if( !class_exists('wordpressIDXFilter'))
?>