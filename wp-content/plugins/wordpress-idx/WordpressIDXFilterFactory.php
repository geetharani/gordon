<?php
if( !class_exists('wordpressIDXFilterFactory')) {
	include_once(   'filter/WordpressIDXFilter.php');
	include_once(   'filter/WordpressIDXFeaturedSearchFilterImpl.php');
	include_once(   'filter/WordpressIDXHotsheetFilterImpl.php');
	include_once(   'filter/WordpressIDXHotsheetListFilterImpl.php');
	
	include_once(   'filter/WordpressIDXAdvancedSearchFormFilterImpl.php');
	include_once(   'filter/WordpressIDXSearchFormFilterImpl.php');
	include_once(   'filter/WordpressIDXQuickSearchFormFilterImpl.php');
	include_once(   'filter/WordpressIDXSearchResultsFilterImpl.php');
	include_once(   'filter/WordpressIDXListingDetailFilterImpl.php');
	
	include_once(   'filter/WordpressIDXOrganizerLoginFormFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerLogoutFilterImpl.php');	
	include_once(   'filter/WordpressIDXOrganizerLoginSubmitFilterImpl.php');
	
	include_once(   'filter/WordpressIDXOrganizerEditSavedSearchFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerEditSavedSearchFormFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerEmailUpdatesConfirmationFilterImpl.php');	
	include_once(   'filter/WordpressIDXOrganizerDeleteSavedSearchFilterImpl.php');
	
	include_once(   'filter/WordpressIDXOrganizerViewSavedSearchFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerViewSavedSearchListFilterImpl.php');

	include_once(   'filter/WordpressIDXOrganizerViewSavedListingListFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerResendConfirmationFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerActivateSubscriberFilterImpl.php');
	include_once(   'filter/WordpressIDXOrganizerSendSubscriberPasswordFilterImpl.php');	

	/** 
	 * This singleton class creates instances of WordpressIDX filters, based
	 * on a type parameter.
	 * @author wordpressIDX
	 */
	class wordpressIDXFilterFactory {
	
		private static $instance ;
		
		private function __construct(){
		}
		
		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXFilterFactory();
			}
			return self::$instance;		
		}
		
		////////////////////////////////////////////////////////
		//Types used to determine the filter type in WordpressIDXFilterFactory.
		const LISTING_SEARCH_RESULTS="idx-results";
		const LISTING_DETAIL="idx-detail";
		const LISTING_SEARCH_FORM="idx-search";
		const LISTING_QUICK_SEARCH_FORM="idx-quick-search";
		const LISTING_ADVANCED_SEARCH_FORM="idx-advanced-search";
		const FEATURED_SEARCH="idx-featured-search";
		const HOTSHEET_SEARCH_RESULTS="idx-toppicks";
		const HOTSHEET_LIST="idx-toppicks-list";
		const ORGANIZER_LOGIN="idx-property-organizer-login";
		const ORGANIZER_LOGOUT="idx-property-organizer-logout";
		const ORGANIZER_LOGIN_SUBMIT="idx-property-organizer-submit-login";
		const ORGANIZER_EDIT_SAVED_SEARCH="idx-property-organizer-edit-saved-search";
		const ORGANIZER_EDIT_SAVED_SEARCH_SUBMIT="idx-property-organizer-edit-saved-search-submit";
		const ORGANIZER_EMAIL_UPDATES_CONFIRMATION="idx-property-organizer-email-updates-success";
		const ORGANIZER_DELETE_SAVED_SEARCH="idx-property-organizer-delete-saved-search";
		const ORGANIZER_DELETE_SAVED_SEARCH_SUBMIT="idx-property-organizer-delete-saved-search-submit";
		const ORGANIZER_VIEW_SAVED_SEARCH="idx-property-organizer-view-saved-search";
		const ORGANIZER_VIEW_SAVED_SEARCH_LIST="idx-property-organizer-view-saved-searches";
		const ORGANIZER_VIEW_SAVED_LISTING_LIST="idx-property-organizer-view-saved-listings";
		const ORGANIZER_RESEND_CONFIRMATION_EMAIL="idx-property-organizer-resend-confirmation-email";
		const ORGANIZER_ACTIVATE_SUBSCRIBER ="idx-property-organizer-activate-subscriber";
		const ORGANIZER_SEND_SUBSCRIBER_PASSWORD="idx-property-organizer-send-login";
		///////////////////////////////////////////////////////		

		public function getFilter( $type ){
			$filter ;
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXFilterFactory.getFilter type=' . $type);
			if($type == wordpressIDXFilterFactory::LISTING_SEARCH_RESULTS ){
				$filter = new wordpressIDXSearchResultsFilterImpl();
			}	
			else if($type == wordpressIDXFilterFactory::LISTING_DETAIL ){
				$filter = new wordpressIDXListingDetailFilterImpl();
			}	
			else if( $type == wordpressIDXFilterFactory::FEATURED_SEARCH){
				$filter = new wordpressIDXFeaturedSearchFilterImpl();
			}
			else if( $type == wordpressIDXFilterFactory::LISTING_ADVANCED_SEARCH_FORM){
				$filter = new wordpressIDXAdvancedSearchFormFilterImpl();
			}
	    	else if( $type == wordpressIDXFilterFactory::LISTING_SEARCH_FORM){
				$filter = new wordpressIDXSearchFormFilterImpl();
			}
			else if( $type == wordpressIDXFilterFactory::LISTING_QUICK_SEARCH_FORM){
				$filter = new wordpressIDXQuickSearchFormFilterImpl();
			}			
			else if( $type == wordpressIDXFilterFactory::HOTSHEET_SEARCH_RESULTS ){
				$filter = new wordpressIDXHotsheetFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::HOTSHEET_LIST ){
				$filter = new wordpressIDXHotsheetListFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_LOGIN ){
				$filter = new WordpressIDXOrganizerLoginFormFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_LOGOUT ){
				$filter = new WordpressIDXOrganizerLogoutFilterImpl() ;
			}			
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_LOGIN_SUBMIT ){
				$filter = new WordpressIDXOrganizerLoginSubmitFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_EDIT_SAVED_SEARCH ){
				$filter = new wordpressIDXOrganizerEditSavedSearchFormFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_EMAIL_UPDATES_CONFIRMATION ){
				$filter = new wordpressIDXOrganizerEmailUpdatesConfirmationFilterImpl() ;
			}			
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_EDIT_SAVED_SEARCH_SUBMIT ){
				$filter = new wordpressIDXOrganizerEditSavedSearchFilterImpl() ;
			}			
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_DELETE_SAVED_SEARCH_SUBMIT ){
				$filter = new wordpressIDXOrganizerDeleteSavedSearchFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_VIEW_SAVED_SEARCH ){
				$filter = new wordpressIDXOrganizerViewSavedSearchFilterImpl() ;
			}				
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_VIEW_SAVED_SEARCH_LIST ){
				$filter = new wordpressIDXOrganizerViewSavedSearchListFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_VIEW_SAVED_LISTING_LIST ){
				$filter = new wordpressIDXOrganizerViewSavedListingListFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_ACTIVATE_SUBSCRIBER){
				$filter = new WordpressIDXOrganizerActivateSubscriberFilterImpl() ;
			}
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_RESEND_CONFIRMATION_EMAIL ){
				$filter = new wordpressIDXOrganizerResendConfirmationFilterImpl() ;
			}			
			else if( $type == wordpressIDXFilterFactory::ORGANIZER_SEND_SUBSCRIBER_PASSWORD ){
				$filter = new WordpressIDXOrganizerSendSubscriberPasswordFilterImpl() ;
			}			
			wordpressIDXLogger::getInstance()->debug('Complete wordpressIDXFilterFactory.getFilter');
			return $filter ;
		}
	}
}
?>