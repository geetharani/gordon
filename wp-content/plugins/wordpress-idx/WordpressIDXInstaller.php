<?php
if(!class_exists('wordpressIDXInstaller')){
	/**
	 * Singleton implementation of wordpressIDXInstaller
	 *
	 * @author wordpressIDX
	 *
	 */
	class wordpressIDXInstaller{

		private static $instance ;
		private $wpidxRewriteRules ;
		private $wpidxAdmin ;

		private function __construct(){
			$this->wpidxRewriteRules=wordpressIDXRewriteRules::getInstance();
			$this->wpidxAdmin=wordpressIDXAdmin::getInstance();
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXInstaller();
			}
			return self::$instance;
		}

		/**
		 * Function installs the Wordpress IDX plugin
		 * and initializes rewrite rules.
		 */
		public function install() {

		    global $wpdb;

		    $this->wpidxRewriteRules->initialize();
		    $this->wpidxRewriteRules->flushRules();
		    
		    wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'wpidx_expired_transients_cleanup');
		}

		/**
		 * Function removes Wordpress IDX plugin related information.
		 */
		public function remove() {
		    global $wpdb;
		    global $wp_rewrite;

		   	//Clear out any rewrite rules associated with the plugin
		   	$this->wpidxRewriteRules->flushRules();
		   	//Delete the authentication token
		   	$this->wpidxAdmin->deleteAuthenticationToken() ;
		   	
		   	wp_clear_scheduled_hook( array(wordpressIDXCleaner::getInstance(), 'removeExpiredwpidxTransients') );
		}


		/**
		 * Update authentictation and rewrite information after
		 * upgrade
		 */
		public function upgrade(){

			$currentVersion=get_option(wordpressIDXConstants::VERSION_OPTION);

			if( $currentVersion != wordpressIDXConstants::VERSION && $this->wpidxAdmin->previouslyActivated() ){
				$this->wpidxAdmin->updateAuthenticationToken() ;
				$this->wpidxRewriteRules->initialize();
				$this->wpidxRewriteRules->flushRules();

				update_option(wordpressIDXConstants::VERSION_OPTION, wordpressIDXConstants::VERSION );
			}
		}
	}
}//end if(!class_exists('wordpressIDXInstaller'))
?>