<?php
if( !class_exists('wordpressIDXCustomization')) {

	/**
	 *
	 * This singleton class is used to add UI customization
	 *
	 * @author wordpressIDX
	 */
	class wordpressIDXCustomization {

		private static $instance ;

		private function __construct(){
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXCustomization();
			}
			return self::$instance;
		}
		
		function addCustomCSS(){
			$cssOverride=get_option(wordpressIDXConstants::CSS_OVERRIDE_OPTION);
			if( isset( $cssOverride )){
				echo("<style type='text/css'>" . $cssOverride . "</style>");
			}
		}
	}
}
?>