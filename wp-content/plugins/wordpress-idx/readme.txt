=== Wordpress IDX Plugin===


Contributors: ihomefinder


Tags: MLS, IDX, Real Estate, Multiple Listing Service, Wordpress IDX


Requires at least: 3.0


Tested up to: 3.2.1


Stable tag: trunk





Wordpress IDX is the leading IDX / MLS real estate search plugin for WordPress sites. Elegant interface, great SEO and excellent lead capture.





== Description ==





The Wordpress IDX plugin from WordpressIDX adds IDX / MLS real estate search to your WordPress site or blog.





* Impress prospective homebuyers and sellers with an elegant design, large photos, and attractive widgets.


* Raise your site's search engine rankings with SEO-friendly listing content indexed under your own domain.


* Generate more leads with email alerts, a consumer property organizer, user registration prompts, social media links and other engagement tools.


* Customize your site with a collection of property gallery and quicksearch widgets.





IDX property data is copyrighted by Multiple Listing Services, thus an WordpressIDX account is required to activate the Wordpress IDX plugin. To get an WordpressIDX account please <a href="http://www.ihomefinder.com/forms/contact-us/">contact us</a>.





For more details about the Wordpress IDX plugin go to: <a href="http://www.ihomefinder.com/product/wordpress-idx/"><span style="text-decoration: underline;">http://www.ihomefinder.com/product/wordpress-idx/</span></a>





= Wordpress IDX PACKAGES =





= Wordpress IDX  =





The Wordpress IDX base package provides MLS/IDX search, results, and property details pages directly within your WordPress site. This package also includes a Featured Properties page as well as a Featured Properties Gallery widget that can be placed on any page within your site. Other features are outlined below.





= Wordpress IDX Pro =





The Wordpress IDX Pro package includes all the features of the Wordpress IDX base package plus daily Email Alerts, Top Picks pages & widgets, Saved Searches, Listing Galleries for Posts & Pages, User Registration prompts and a complete Mobile IDX website.





= Wordpress IDX PAGES =





The Wordpress IDX plugin creates links to IDX pages that can be added to your WordPress navigation menu and other portions of your site.





The following IDX pages are provided:





= Property Search =





Site visitors can search for properties by price, city, baths, and bedrooms. (see an example at <a href="http://blog.anyhomesrealty.com/homes-for-sale-search/">http://blog.anyhomesrealty.com/homes-for-sale-search/</a>)





= Advanced Property Search =





Site visitors can search for properties using advanced search criteria, which varies by MLS. (see an example at http://blog.anyhomesrealty.com/homes-for-sale-search-advanced/)



= Open Homes Search =





Site visitors can search for open homes by price, city, bedrooms and bathrooms. Within results, they can view  open house date and time information 


= Featured Properties =





The Featured Properties page displays your current listings, or listings from your office if you currently have no listings of your own. (see an example at <a href="http://blog.anyhomesrealty.com/homes-for-sale-featured/">http://blog.anyhomesrealty.com/homes-for-sale-featured/</a>)





= Top Picks =





Top Picks pages display a list of saved searches such as listings in your market area, homes with water views, distressed properties, and so on. (see an example at <a href="http://blog.anyhomesrealty.com/homes-for-sale-toppicks/">http://blog.anyhomesrealty.com/homes-for-sale-toppicks/</a>)


= Sold Listings =




Show your history of success by displaying your sold listings on this page. After properties go off the market, it's easy to select them for display on this page 


= Supplemental Listings =





Display pocket listings, distressed properties,listings that are coming soon or any non-MLS listing on the Supplemental Listings page. Easily add property information, photos and more through the Control Panel


= Listing Gallery Shortcodes =








Insert listings into your Pages and Posts easily. A simple interface allows you to add Featured Listings, Top Picks or Search results directly into your Page or Post using a shortcode. (see an example at <a href="http://blog.anyhomesrealty.com/listing-galleries/">http://blog.anyhomesrealty.com/listing-galleries/</a>) 








= Email Alerts =








Your site visitors can sign-up for daily email alerts containing listings that match their desired search criteria. Email alert sign-ups and activity are tracked in your WordpressIDX Control Panel. (see an example at <a href="http://blog.anyhomesrealty.com/email-alerts/">http://blog.anyhomesrealty.com/email-alerts/</a>)











= Property Organizer  =








The Property Organizer page enables prospective homebuyers to save properties and searches of interest. Saved searches trigger daily email alerts containing properties that match a consumer's desired search criteria. Property organizer sign-ups and activity are tracked in your WordpressIDX Control Panel. You can also add to or modify the listings and searches saved in any consumer's Property Organizer. (see an example at <a href="http://blog.anyhomesrealty.com/property-organizer-login/">http://blog.anyhomesrealty.com/property-organizer-login/</a>)



= Contact Form  =


Allow site visitors to contact you easily with this form. Track these leads through your Control Panel




= Valuation Request  =


Homeowners interested in having you list their home can fill out a Home Valuation Request form to begin the valuation process 






= WIDGETS =








You can enhance your WordPress site with a selection of Quicksearch and Property Gallery widgets. Easily configure these widgets to display only on certain Wordpress IDX pages. Your WordPress theme will determine where the widgets appear on your other WordPress pages. 











= Quick Search Widgets  =








Add property search to any page by dragging the Quicksearch widget to the page's sidebar.





Add a free text search to the body of a page using the shortcode [wordpress_idx_quick_search].








= Featured Properties Gallery Widget =








Display your Featured Properties in an attractive WordPress widget on any page of your site.











= Top Picks Gallery Widget =








Display the results of a saved search created in your WordpressIDX Control Panel. Showcase the type of listings you specialize in, such as REOs, condos, or waterfront properties. Or, highlight the different neighborhoods and subdivisions that you cover. (Wordpress IDX Pro only)











= Saved Search Gallery Widget  =








Similar to a Top Picks Gallery Widget, except you can define the saved search using basic search parameters within your WordPress Dashboard rather than the WordpressIDX Control Panel. (Wordpress IDX Pro only)











= Saved Search Link Widget  =








Add links to pages containing your saved searches. For example, add a link to all active listings in every neighbor of your territory. This widget is configured in the same way as the Saved Search Gallery Widget, but instead of showing a gallery of listings, it displays a link to a saved search results page. (Wordpress IDX Pro only)







= Slideshow Widgets  =



Slideshows displaying Featured or Top Picks listings (Wordpress IDX Pro only) can be added to any website page.









= WORDPRESS THEMES =











A WordPress Theme defines the layout and design of your WordPress website. Wordpress IDX is known to be compatible with the following themes:











* StudioPress AgentPress





* Twenty Ten





* Twenty Eleven





* Headway 2012











Technical notes: 





Other WordPress themes are likely to display Wordpress IDX content correctly provided that the theme is properly coded with jQuery set to run in 'no conflict' mode. Please contact WordpressIDX support (<a href="mailto:support@ihomefinder.com">support@ihomefinder.com</a>) for assistance with specific themes.








Wordpress IDX pages are 620 pixels wide, and widgets are 180 pixels wide.











= CONTROL PANEL =








Your Wordpress IDX pages display IDX pages as set-up in your WordpressIDX Control Panel. Note that this is separate from your WordPress Dashboard. Use the Control Panel to configure property search settings, Top Picks, Featured Properties, branding for Email Alerts, lead management and much more. The Help system, located in the top right corner of the Control Panel, provides detailed instructions for all account features.








To learn more: http://www.ihomefinder.com/product/wordpress-idx/











== Installation ==











[Click here to open the Wordpress IDX Guide](http://blog.ihomefinder.com/wordpress-idx-guide/)











Before you get started ensure you have the following:











* WordPress





* WordpressIDX Wordpress IDX registration key (Your registration key is provided by WordpressIDX when you sign up. This can also be found in your account control panel.)

















= Install WordPress =











You need to install WordPress on your own hosted website.











If you do not have your own hosted WordPress page, please see if your web hosting company provides a quick installation method.











If they do not you will need to visit 'WordPress.org' and navigate to the 'download page'.





Here is the [link to download](http://wordpress.org/download/), and there is also a [handy guide](http://codex.wordpress.org/Installing_WordPress) which will help you in this process.

















= Add and Activate Plugin =











Login to the Wordpress as an administrator.











In the Plugins menu, select 'Add New' and 'Search'. Search for the term 'Wordpress IDX' and select 'Install Now'. This will install Wordpress IDX from the Wordpress repository and you will receive notification if the plugin requires updates.























= Register =











1. To obtain your Registration Key, go to your WordpressIDX Control Panel and Navigate to Account Setup > General Settings. Find the section titled 'Registration Key'. Highlight this key and then right click > Copy.











2. Go back to your WordPress Admin panel and navigate the left menu to find Wordpress IDX Information > Register. Paste in your registration key and click 'Save Changes'.























= Modify Permalinks =











The last step you must perform before you can customize anything is to change your permalinks. Navigate to Settings &gt; Permalinks.











From here you must choose a permalink setting other than "Default".











Once you have selected a different setting (eg. 'Day and Name'), click on Save Changes.











You now have successfully installed Wordpress IDX into WordPress.

















== Frequently Asked Questions ==





 





= Will the plugin work after I download and install it? =











The plugin will not work until you have set up an [WordpressIDX Wordpress IDX](http://www.ihomefinder.com/product/wordpress-idx/) account. The Wordpress IDX account will provide the required registration key that enables the plugin to work. Please <a href="http://www.ihomefinder.com/forms/contact-us/">contact us</a> if you have questions about account options or if you would like to sign up for an account.

















= Will my site get SEO credit for IDX listings? =











Yes. Wordpress IDX doesn't "frame" property listing data into your WordPress site. Instead, listing content appears directly under your domain, ensuring that your site gets full SEO credit.

















= What kind of lead capture tools are provided? =











The base Wordpress IDX service offers a form to request for more information on a property and a form to schedule a showing. A consumer Property Organizer is also provided to enable site visitors to save listings of interest. The Wordpress IDX Pro service adds an automated daily Email Alert feature. Prospective homebuyers are automatically emailed listings that match their specified criteria.

















= Can anyone get an WordpressIDX Wordpress IDX account? =











Only real estate professionals who are active members of a [supported MLS](http://www.ihomefinder.com/mls-coverage/) can sign-up for an Wordpress IDX account. If you you don't see your MLS listed, please <a href="http://www.ihomefinder.com/forms/contact-us/">contact us</a> about adding service for your MLS.

















= Do I have to contact my MLS when signing up for an Wordpress IDX account? =











Our automated IDX licensing will send you any necessary paperwork when you request an account. When completed and returned to us, we will manage your paperwork through the MLS approval process.











= Where can I find out if my MLS is supported? =











Please refer to our current list of [supported MLSs](http://www.ihomefinder.com/mls-coverage/).

















= Does the plugin provide access to multiple MLSs? =











The plugin will provide IDX data for the MLS(s) you request when you sign up for your Wordpress IDX account. If you belong to more than one MLS, you can request service for additional MLSs at an additional cost.

















= How long does it take to get MLS approval? =











Depending upon your MLS, it can take anywhere from less than a business day to a week. While you're waiting for MLS approval you can test Wordpress IDX on your site with our demo MLS data.

















= How frequently is the data updated? =











Our listing data is updated every 2 hours for most MLSs, and as frequently as every 15 minutes for some MLSs.

















= Where can I learn more about the Wordpress IDX features and pricing? =











Please visit the [Wordpress IDX page](http://www.ihomefinder.com/product/wordpress-idx/) on our website for the latest news and information.

















= Will Wordpress IDX work with my WordPress theme? =











Wordpress IDX is known to be compatible with the following themes:











* StudioPress AgentPress





* Twenty Ten





* Twenty Eleven





* Headway 2012











Other WordPress themes are likely to display Wordpress IDX content correctly provided that the theme is properly coded with jQuery set to run in 'no conflict' mode.











= Will Wordpress IDX work with a mobile device? =











The Pro package includes a full mobile IDX site that will work on any browser-enabled mobile device. We can provide you with a mobile re-direct plugin that will automatically send mobile devices to your mobile IDX site. For more information visit our <a href="http://www.ihomefinder.com/product/wordpress-idx/wordpress-idx-features/mobile-site/">mobile IDX page</a>.

















== Screenshots ==





1. Great SEO Wordpress IDX delivers the powerful features of WordpressIDX IDX directly on your WordPress website. WordpressIDX IDX makes sure all listing data and pages in your account are indexed by search engines and WordPress is also designed for Search Engine Optimization (SEO). The combination of WordPress and Wordpress IDX will provide you with a website with IDX that search engines will love.


2. Elegant Interface &amp; Gorgeous Property Photos. A picture is worth a thousand words, and the photos in Wordpress IDX say it all. A simple and elegant interface with beautiful photos is what you'll find with Wordpress IDX.


3. Quicksearch &amp; Property Gallery widgets. Website visitors will love how easy it is to find the information they're looking for with features like Quicksearch &amp; Property Gallery widgets.


4. Facebook &amp; Twitter Integration. With a click of the Share button, website visitors or realtors themselves can share property details on social media networks. Sharing of listings on social media accounts also provides valuable backlinks to your site which benefits your SEO.


5. Mobile MLS Search With the Wordpress IDX Pro package, you'll get a Mobile MLS Search site. It looks and feels like an app but works right away on mobile devices with a browser. Please note that with Wordpress IDX Pro, you'll need to download a plugin so that when visitors come to your site on their mobile phone, they will be automatically re-directed to your mobile site.


6. Email Alerts. Email Alerts are a great way of staying connected to your prospects and delivering value to them. Each time a property that matches their saved search criteria comes to market, they'll receive an email with your name and brand giving them the information they want and need.











== Changelog ==

= 1.3.5 =

* Fixed issue related to user registration on the top picks page.

= 1.3.4 =

* Fixed issue related to Email Alerts when passwords are required.


= 1.3.3 =

* Added ability to customize CSS for Property Gallery Slider

* Added spam-blocking functionality to forms

* Addressed some theme compatibility issues


= 1.3.2 =

* Added Broker & Sold/Supplemental pages to widget configuration

* Fixed issues with Light & Ultra-Light User Registration


= 1.3.1 =

* Updated Listing Gallery shortcodes to optionally include a map.


= 1.3.0 =

* Added Broker Features


= 1.2.0 =

* Added new pages: Contact Form, Home Valuation Request, Open Homes Search, Supplemental Listings, Sold Listings
* Added support for display of Sold and Supplemental listings on Featured Properties page
* Added option to use full-size photo in results (requires custom CSS to display on page)
* Search form price fields clear on click
* Improved error messaging for client log-in with accounts flagged for deletion


= 1.1.7 =

* Added navigation bar for Property Organizer
* Added link to view matches from Saved Searches within Property Organizer
* Added Help page to Property Organizer, to be used as default home when no saved properties or searches exist
* Improved error handling on Property Organizer signup
* Interactive Map Search Beta


= 1.1.6 =

* Added Free Text field in Search, Advanced Search and Email Alert sign-up

* Support for Chicago neighborhoods in Midwest Real Estate Data MLS

* Default search prices changed to No Min/No Max

* Improved Unsubscribe process from email updates

* Added support for template change for all IDX pages

* Overall performance improvements & decreased page load time on property details pages

* Fixed modal positioning for certain themes

* Allowed for Featured listings to appear first in search results

* Reconfigured search widget to allow customization of spacing through CSS

* Added full support for template changes in Twenty Eleven theme

* Improved error handling on property gallery wizard

* Added address to GreatSchools widget to improve widget map position


= 1.1.5 =

* Updated authentication synchronization code to fix periodic authentication issues.


= 1.1.4 =

* Added ability to modify IDX Page Permalinks


* Added ability to edit IDX Page Titles


* Added ability to select IDX Page Template


* Saved properties can be deleted from Property Organizer


* Fixed pagination and Next/Previous navigation issues on Advanced Search results and Top Picks pages


= 1.1.3 =

* Added Advanced Search criteria to email updates


* Added paging to Next/Previous from property details results


* Fixed Multiple Agent ID issue in Advanced Search


* Fixed paging problem in Advanced Search results


* Fixed widget display issue on Advanced Search page


* Fixed disclosure & map links in Advanced Search


* Advanced Search option is now hidden for property types that do not offer advanced search


* Bed/Bath search fields are deactivated for Commercial property searches


* Removed 'New Search' link from Top Picks pages


* Fixed display problem with Genesis ���?�??Featured Posts���?�? widget

= 1.1.2 =

* Added Listing Gallery display through shortcode on Pages and Posts


* Added IDX page display selection to Widgets 


* Added CSS override ability


* Fixed open home display bug


* Fixed Advanced Search multi-select bug








= 1.1.0 =





* Added User Registration prompts to collect user information at customizable trigger points (Ultra-light, Light & Heavy options available)


* Added Advanced Search as well as link to Advanced Search from standard Search page


* Added click-through login from email notifications


* Improved subscriber activity tracking 


* Fixed text display bugs on Top Picks pages


= 1.0.0 =

* Initial release.


== Upgrade Notice ==

= 1.3.5 =

* Update to fix user registration on the top picks page

= 1.3.4 =

Update to fix Email Alerts bug.


= 1.3.3 =

Updated CSS configuration for Property Gallery Slider and added spam blocking code.


= 1.3.2 =

Added Broker & Sold/Supplemental pages to widget configuration and fixed User Registration issues.


= 1.3.1 =

Updated Listing Gallery shortcodes to optionally include a map.


= 1.3.0 =

Broker Features available.


= 1.2.0 = 

The latest version of Wordpress IDX includes two new contact forms, Supplemental & Sold Listings pages and Open Homes Search.



= 1.1.7 = 

The latest version of Wordpress IDX includes many improvements to the Property Organizer.




= 1.1.6 = 



The latest version of Wordpress IDX features Search interface upgrades, performance improvements and numerous bug fixes.



= 1.1.4 =





The latest version of Wordpress IDX adds ability to modify IDX Page Permalinks, edit Titles, and select Template.








= 1.1.3 =





The latest version of Wordpress IDX adds Advanced Search to email updates and fixes a number of bugs.





= 1.2.0 =











The latest version of Wordpress IDX adds Listing Gallery Shortcodes, display options for widgets and a CSS override for Wordpress IDX pages.





= 1.1.0 =











The latest version of Wordpress IDX supports Advanced Search, click-through login from email notifications, improved activity tracking and User Registration prompts for Pro users.



































