<?php
if( !class_exists('wordpressIDXUtility')) {
	/**
	 *
	 *
	 * @author wordpressIDX
	 */
	class wordpressIDXUtility {

		private static $instance ;

		private function __construct(){
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXUtility();
			}
			return self::$instance;
		}

		public function getQueryVar($name){
			global $wp;
			$result = $this->getVarFromArray( $name, $wp->query_vars ) ;
			return $result ;
		}

		public function getRequestVar($name){
			$result = $this->getVarFromArray( $name, $_REQUEST ) ;
			return $result ;
		}

		public function getVarFromArray($name, $arrayVar){
			$result=null ;
			if( array_key_exists($name, $arrayVar)){
				$result = $arrayVar[$name];
			}
			return $result ;
		}
		
		/**
		 * When navigating listing detail pages, we need to set the next and previous
		 * details and pass in the request, to properly create next and previous links
		 * 
		 * @param string $wpidxUrl
		 * @param int $boardId
		 * @param string $listingNumber
		 */
		public function setPreviousAndNextInformation( $wpidxUrl, $boardId, $listingNumber ){
			$searchSummaryArray = wordpressIDXStateManager::getInstance()->getSearchSummary() ;
			$key= $boardId . "|" . $listingNumber ;
			if( isset( $searchSummaryArray )){
				$searchSummaryObject = $searchSummaryArray[ $key ];				
				if( isset( $searchSummaryObject )){
					if( isset($searchSummaryObject->previousId)){
						$searchSummaryPrevious = $searchSummaryArray[ $searchSummaryObject->previousId ];
						$prevBoardAndListingNumber = explode("|", $searchSummaryObject->previousId );
						$wpidxUrl .= "&prevBoardId=" . $prevBoardAndListingNumber[0]; ;					
						$wpidxUrl .= "&prevListingNumber=" . $prevBoardAndListingNumber[1]; ;
						$wpidxUrl .= "&prevAddress=" . urlencode($searchSummaryPrevious->address) ;
						$wpidxUrl .= "&prevStatus=" . urlencode($searchSummaryPrevious->status) ;
					}
					
					if( isset($searchSummaryObject->nextId)){
						$searchSummaryNext = $searchSummaryArray[ $searchSummaryObject->nextId ];
						$nextBoardAndListingNumber = explode("|", $searchSummaryObject->nextId );
						$wpidxUrl .= "&nextBoardId=" . $nextBoardAndListingNumber[0] ;					
						$wpidxUrl .= "&nextListingNumber=" . $nextBoardAndListingNumber[1] ;
						$wpidxUrl .= "&nextAddress=" . urlencode($searchSummaryNext->address) ;
						$wpidxUrl .= "&nextStatus=" . urlencode($searchSummaryNext->status) ;
					}
				}	
			}
			
			return $wpidxUrl ;
		}

		/**
		 * Returns true is the user agent is a known web crawler
		 * @return boolean
		 */
		public function isWebCrawler(){
			$result=true;
			$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);			
			$knownCrawlersArray 
				= array("Mediapartners-Google","Googlebot","Baiduspider","Bingbot","msnbot","Slurp","Twiceler","YandexBot");			
			foreach ($knownCrawlersArray as $value ){
				if( strpos($userAgent, $value)){
					$result=true;
					break;
				}
			}
			return $result ;
		}
		
		/**
		 * 
		 * Return true if the string is empty, else return false
		 * @param unknown_type $value
		 */
		public function isStringEmpty( $value ){
			$result=true;
			
			if( $value != null && strlen($value) > 0){
				$result=false;
			}
			return $result;
		}
	}
}
?>