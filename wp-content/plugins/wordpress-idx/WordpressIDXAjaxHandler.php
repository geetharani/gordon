<?php
if( !class_exists('wordpressIDXAjaxHandler')) {
	/**
	 * This class is handle all WordpressIDX Ajax Requests.
	 * It proxies the requests and returns the proper results.
	 *
	 * @author wordpressIDX
	 */
	class wordpressIDXAjaxHandler {

		private static $instance ;
		private $wpidxAdmin ;

		private function __construct(){
			$this->wpidxAdmin = wordpressIDXAdmin::getInstance();
		}
		
		private function isSpam(){
			$isSpam=true;			
			$spamChecker = wordpressIDXUtility::getInstance()->getRequestVar('JKGH00920');
			if( wordpressIDXUtility::getInstance()->isStringEmpty($spamChecker)){
				//if spamChecker is NOT empty, then we know this is SPAM
				$isSpam=false ;
			}
			return $isSpam ;
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXAjaxHandler();
			}
			return self::$instance;
		}

		public function requestMoreInfo(){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAjaxHandler.requestMoreInfo');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();
			
			$isSpam=$this->isSpam() ;			
			if( $isSpam == false ){
				$action = wordpressIDXUtility::getInstance()->getRequestVar('action');
				$listingNumber = wordpressIDXUtility::getInstance()->getRequestVar('listingNumber');
				$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');
				$interestLevel = wordpressIDXUtility::getInstance()->getRequestVar('interestLevel');
				$name = wordpressIDXUtility::getInstance()->getRequestVar('name');
				$phone = wordpressIDXUtility::getInstance()->getRequestVar('phone');
				$email = wordpressIDXUtility::getInstance()->getRequestVar('email');
				$password = wordpressIDXUtility::getInstance()->getRequestVar('password');
				$message = wordpressIDXUtility::getInstance()->getRequestVar('message');
				
				$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=request-more-info' ;
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "action", $action);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "listingNumber", $listingNumber);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "interestLevel", $interestLevel);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $name);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone", $phone);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "message", $message);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
	
				$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
				$content = wordpressIDXRequestor::getContent( $contentInfo );
				wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
				wordpressIDXLogger::getInstance()->debug('End wordpressIDXAjaxHandler.requestMoreInfo');
	
				echo $content ;
			}
			die();
		}

		public function scheduleShowing(){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAjaxHandler.scheduleShowing');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();
			
			$isSpam=$this->isSpam() ;
			if( $isSpam == false  ){
				$action = wordpressIDXUtility::getInstance()->getRequestVar('action');
				$listingNumber = wordpressIDXUtility::getInstance()->getRequestVar('listingNumber');
				$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');
	
				$name = wordpressIDXUtility::getInstance()->getRequestVar('name');
				$phone = wordpressIDXUtility::getInstance()->getRequestVar('phone');
				$phoneAlt = wordpressIDXUtility::getInstance()->getRequestVar('phone_alt');
				$email = wordpressIDXUtility::getInstance()->getRequestVar('email');
				$password = wordpressIDXUtility::getInstance()->getRequestVar('password');
				$comments = wordpressIDXUtility::getInstance()->getRequestVar('comments');
				$prefDate = wordpressIDXUtility::getInstance()->getRequestVar('prefDate');
				$prefTime = wordpressIDXUtility::getInstance()->getRequestVar('prefTime');
				$altDate = wordpressIDXUtility::getInstance()->getRequestVar('altDate');
				$altTime = wordpressIDXUtility::getInstance()->getRequestVar('altTime');
	
				$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=schedule-showing' ;
	
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "listingNumber", $listingNumber);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $name);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone", $phone);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone_alt", $phoneAlt);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "comments", $comments);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "prefDate", $prefDate);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "prefTime", $prefTime);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "altDate", $altDate);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "altTime", $altTime);
				$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
	
				$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
				$content = wordpressIDXRequestor::getContent( $contentInfo );
	
				wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
				wordpressIDXLogger::getInstance()->debug('End wordpressIDXAjaxHandler.scheduleShowing');
	
				echo $content ;
					
			}
			die();
		}

		public function photoTour(){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAjaxHandler.photoTour');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();

			$action = wordpressIDXUtility::getInstance()->getRequestVar('action');
			$listingNumber = wordpressIDXUtility::getInstance()->getRequestVar('listingNumber');
			$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');

			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=photo-tour' ;

			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "listingNumber", $listingNumber);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);

			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
			$content = wordpressIDXRequestor::getContent( $contentInfo );

			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAjaxHandler.photoTour');

			echo $content ;
			die();
		}

		public function saveProperty(){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAjaxHandler.saveProperty');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();

			$action = wordpressIDXUtility::getInstance()->getRequestVar('action');
			$listingNumber = wordpressIDXUtility::getInstance()->getRequestVar('listingNumber');
			$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');
			$interestLevel = wordpressIDXUtility::getInstance()->getRequestVar('interestLevel');
			$name = wordpressIDXUtility::getInstance()->getRequestVar('name');
			$phone = wordpressIDXUtility::getInstance()->getRequestVar('phone');
			$email = wordpressIDXUtility::getInstance()->getRequestVar('email');
			$password = wordpressIDXUtility::getInstance()->getRequestVar('password');
			$actionType = wordpressIDXUtility::getInstance()->getRequestVar('actionType');
			$subscriberId = wordpressIDXUtility::getInstance()->getRequestVar('subscriberID');

			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=save-property' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "action", $action);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "actionType", $actionType);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "listingNumber", $listingNumber);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "interestLevel", $interestLevel);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $name);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone", $phone);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
			wordpressIDXLogger::getInstance()->debugDumpVar($contentInfo);
			

			$content = wordpressIDXRequestor::getContent($contentInfo);
			echo $content ;
			die();
		}

		public function saveSearch(){
			wordpressIDXLogger::getInstance()->debug('Begin wordpressIDXAjaxHandler.saveSearch');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();

			$action = wordpressIDXUtility::getInstance()->getRequestVar('action');
			$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');
			$interestLevel = wordpressIDXUtility::getInstance()->getRequestVar('interestLevel');
			$name = wordpressIDXUtility::getInstance()->getRequestVar('name');
			$phone = wordpressIDXUtility::getInstance()->getRequestVar('phone');
			$email = wordpressIDXUtility::getInstance()->getRequestVar('email');
			$password = wordpressIDXUtility::getInstance()->getRequestVar('password');
			$actionType = ""  ;
			$method=wordpressIDXUtility::getInstance()->getRequestVar('method');

			if( $method == "createNewAccountAndSaveSearch"){
				$actionType="newaccount";
			}

			$subscriberId=wordpressIDXUtility::getInstance()->getRequestVar('subscriberID');
			if( empty( $subscriberId )){
				
			}

			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=save-search' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "action", $action);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "actionType", $actionType);
			//$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID);
			//$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "interestLevel", $interestLevel);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberName", $name);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phone", $phone);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "modal", "true");
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "subscriberId", $subscriberId);

			//we need to initialize here for Ajax requests, when trying to save a search
			wordpressIDXStateManager::getInstance()->initialize();
			$lastSearchQueryString = wordpressIDXStateManager::getInstance()->getLastSearchQueryString();
			$lastSearchQueryString = str_replace('[]', '', $lastSearchQueryString );
			$lastSearchQueryString = str_replace('%5B%5D', '', $lastSearchQueryString );
			$wpidxUrl .= '&' . $lastSearchQueryString ;
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);

			$content = wordpressIDXRequestor::getContent( $contentInfo );

			wordpressIDXLogger::getInstance()->debugDumpVar($contentInfo );

			wordpressIDXLogger::getInstance()->debug( '<br/><br/>' . $wpidxUrl ) ;
			wordpressIDXLogger::getInstance()->debug('End wordpressIDXAjaxHandler.saveSearch');

			echo $content ;
			die();
		}
		
		public function advancedSearchMultiSelects(){
			wordpressIDXLogger::getInstance()->debug('Begin advancedSearchMultiSelects');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken();
			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=advanced-search-multi-select-values' ;
			$wpidxUrl = WordpressIDXRequestor::addVarsToUrl($wpidxUrl, $_REQUEST) ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true" );
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
			$content = wordpressIDXRequestor::getContent( $contentInfo );

			echo $content ;
			wordpressIDXLogger::getInstance()->debug('End advancedSearchMultiSelects');
			die();
		}
		
		public function getAdvancedSearchFormFields(){
			wordpressIDXLogger::getInstance()->debug('Begin getAdvancedSearchFormFields');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken() ;
			
			$boardID = wordpressIDXUtility::getInstance()->getRequestVar('boardID');
			
			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=advanced-search-fields' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "boardID", $boardID );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true" );
			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);

			$content = wordpressIDXRequestor::getContent( $contentInfo );

			echo $content ;
			wordpressIDXLogger::getInstance()->debug('End getAdvancedSearchFormFields');
			die();
		}		
		
		public function leadCaptureLogin(){
			wordpressIDXLogger::getInstance()->debug('Begin leadCaptureLogin');
			$authenticationToken=$this->wpidxAdmin->getAuthenticationToken() ;
			
			$leadCaptureId = wordpressIDXUtility::getInstance()->getRequestVar('leadCaptureId');
			$email = wordpressIDXUtility::getInstance()->getRequestVar('wpidxEmail');
			$name = wordpressIDXUtility::getInstance()->getRequestVar('wpidxName');
			$telephone = wordpressIDXUtility::getInstance()->getRequestVar('wpidxTelephone');
			$password = wordpressIDXUtility::getInstance()->getRequestVar('wpidxPassword');
			$agentId = wordpressIDXUtility::getInstance()->getRequestVar('agentID');		

			$type = wordpressIDXUtility::getInstance()->getRequestVar('wpidxType');
			
			$wpidxUrl = WordpressIDXConstants::EXTERNAL_URL . '?method=handleRequest&viewType=json&requestType=lead-capture-login' ;
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "authenticationToken", $authenticationToken );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "phpStyle", "true" );
			
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "loginType", $type );
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "email", $email);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "name", $name);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "telephone", $telephone);
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "password", $password);	
			$wpidxUrl = WordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "agentID", $agentId);
			$wpidxUrl = wordpressIDXRequestor::appendQueryVarIfNotEmpty($wpidxUrl, "leadCaptureId", $leadCaptureId );		
			
			//echo $wpidxUrl ;
			//die();

			
			$contentInfo = wordpressIDXRequestor::remoteRequest($wpidxUrl, true);
			//var_dump($contentInfo);
			$content = wordpressIDXRequestor::getContent( $contentInfo );
			echo $content ;
			wordpressIDXLogger::getInstance()->debug('End getAdvancedSearchFormFields');
			die();
		}				
	}//end class
}//end ifclass_exists
?>