<?php
if( !class_exists('wordpressIDXVirtualPageHelper')) {
	/**
	 * @author wordpressIDX
	 * 
	 * This class defines option names related to WordpressIDX Virtual Pages. 
	 * 
	 * For most virtual pages, we store the following options:
	 * - title - page title used by the theme
	 * - template - used by the theme to display the virual page.
	 * - permalink - used in the rewrite rules.  
	 */
	class wordpressIDXVirtualPageHelper {

		//Group for virtual page related options
		const OPTION_VIRTUAL_PAGE_CONFIG="wpidx-virtual-page-config";
		
		//Default template for Virtual Pages that do not have a template.
		const OPTION_VIRTUAL_PAGE_TEMPLATE_DEFAULT="wpidx-virtual-page-template-default";
		
		//Listing DetailVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_DETAIL="wpidx-virtual-page-title-detail";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_DETAIL="wpidx-virtual-page-template-detail";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_DETAIL="wpidx-virtual-page-permalink-text-detail";	

		//Listing Search VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_SEARCH="wpidx-virtual-page-title-search";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_SEARCH="wpidx-virtual-page-template-search";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SEARCH="wpidx-virtual-page-permalink-text-search";	
		
		//Advanced Listing Search VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_ADV_SEARCH="wpidx-virtual-page-title-adv-search";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_ADV_SEARCH="wpidx-virtual-page-template-adv-search";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ADV_SEARCH="wpidx-virtual-page-permalink-text-adv-search";	
				
		//Organizer Login VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_ORG_LOGIN="wpidx-virtual-page-title-org-login";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_ORG_LOGIN="wpidx-virtual-page-template-org-login";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ORG_LOGIN="wpidx-virtual-page-permalink-text-org-login";	

		//Email Updated VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_EMAIL_UPDATES="wpidx-virtual-page-title-email-updates";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_EMAIL_UPDATES="wpidx-virtual-page-template-email-updates";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_EMAIL_UPDATES="wpidx-virtual-page-permalink-text-email-updates";	

		//Featured VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_FEATURED="wpidx-virtual-page-title-featured";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_FEATURED="wpidx-virtual-page-template-featured";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_FEATURED="wpidx-virtual-page-permalink-text-featured";	

		//Hotsheet VirtualPage Options
		const OPTION_VIRTUAL_PAGE_TITLE_HOTSHEET="wpidx-virtual-page-title-hotsheet";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_HOTSHEET="wpidx-virtual-page-template-hotsheet";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_HOTSHEET="wpidx-virtual-page-permalink-text-hotsheet";

		//Contact Form Virtual Page Options
		const OPTION_VIRTUAL_PAGE_TITLE_CONTACT_FORM="wpidx-virtual-page-title-contact-form";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_CONTACT_FORM="wpidx-virtual-page-template-contact-form";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_CONTACT_FORM="wpidx-virtual-page-permalink-text-contact-form";

		//Valuation Form Virtual Page Options
		const OPTION_VIRTUAL_PAGE_TITLE_VALUATION_FORM="wpidx-virtual-page-title-valuation-form";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_VALUATION_FORM="wpidx-virtual-page-template-valuation-form";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_VALUATION_FORM="wpidx-virtual-page-permalink-text-valuation-form";

		//Open Home Search Form Virtual Page Options
		const OPTION_VIRTUAL_PAGE_TITLE_OPEN_HOME_SEARCH_FORM="wpidx-virtual-page-title-open-home-search-form";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_OPEN_HOME_SEARCH_FORM="wpidx-virtual-page-template-open-home-search-form";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OPEN_HOME_SEARCH_FORM="wpidx-virtual-page-open-home-search-form";
		
		//Featured Sold Listings Virtual Page Options
		const OPTION_VIRTUAL_PAGE_TITLE_SOLD_FEATURED="wpidx-virtual-page-title-sold-featured";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_SOLD_FEATURED="wpidx-virtual-page-template-sold-featured";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SOLD_FEATURED="wpidx-virtual-page-permalink-text-sold-featured";
		
		//Supplemental listings
		const OPTION_VIRTUAL_PAGE_TITLE_SUPPLEMENTAL_LISTING="wpidx-virtual-page-title-supplemental-listing";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_SUPPLEMENTAL_LISTING="wpidx-virtual-page-template-supplemental-listing";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SUPPLEMENTAL_LISTING="wpidx-virtual-page-permalink-text-supplemental-listing";

		//Listing SoldDetailVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_SOLD_DETAIL="wpidx-virtual-page-title-sold-detail";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_SOLD_DETAIL="wpidx-virtual-page-template-sold-detail";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SOLD_DETAIL="wpidx-virtual-page-permalink-text-sold-detail";		

		//Listing OfficeListVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_OFFICE_LIST="wpidx-virtual-page-title-office-list";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_LIST="wpidx-virtual-page-template-office-list";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_LIST="wpidx-virtual-page-permalink-text-office-list";		

		//Listing OfficeDetailVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_OFFICE_DETAIL="wpidx-virtual-page-title-office-detail";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_DETAIL="wpidx-virtual-page-template-office-detail";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_DETAIL="wpidx-virtual-page-permalink-text-office-detail";		

		//Listing AgentListVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_AGENT_LIST="wpidx-virtual-page-title-agent-list";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_LIST="wpidx-virtual-page-template-agent-list";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_LIST="wpidx-virtual-page-permalink-text-agent-list";		

		//Listing AgentDetailVirtualPage related options
		const OPTION_VIRTUAL_PAGE_TITLE_AGENT_DETAIL="wpidx-virtual-page-title-agent-detail";
		const OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_DETAIL="wpidx-virtual-page-template-agent-detail";
		const OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_DETAIL="wpidx-virtual-page-permalink-text-agent-detail";		
		
		private static $instance ;

		private function __construct(){
		}

		public static function getInstance(){
			if( !isset(self::$instance)){
				self::$instance = new wordpressIDXVirtualPageHelper();
			}
			return self::$instance;
		}
		
		public function registerOptions(){
			//Virtual Page settings
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_DEFAULT );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_DETAIL );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_SEARCH );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_SEARCH );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SEARCH );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_ADV_SEARCH );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_ADV_SEARCH );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ADV_SEARCH );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_ORG_LOGIN );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_ORG_LOGIN );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_ORG_LOGIN );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_EMAIL_UPDATES );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_EMAIL_UPDATES );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_EMAIL_UPDATES );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_FEATURED);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_FEATURED );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_FEATURED );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_HOTSHEET);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_HOTSHEET );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_HOTSHEET );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_CONTACT_FORM);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_CONTACT_FORM );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_CONTACT_FORM );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_VALUATION_FORM);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_VALUATION_FORM );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_VALUATION_FORM );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_OPEN_HOME_SEARCH_FORM);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_OPEN_HOME_SEARCH_FORM );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OPEN_HOME_SEARCH_FORM);

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_SUPPLEMENTAL_LISTING);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_SUPPLEMENTAL_LISTING );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SUPPLEMENTAL_LISTING );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_SOLD_FEATURED);
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_SOLD_FEATURED );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SOLD_FEATURED );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_SOLD_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_SOLD_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_SOLD_DETAIL );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_OFFICE_LIST );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_LIST );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_LIST );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_OFFICE_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_OFFICE_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_OFFICE_DETAIL );

			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_AGENT_LIST );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_LIST );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_LIST );
			
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TITLE_AGENT_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_AGENT_DETAIL );
			register_setting( wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_CONFIG, wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_PERMALINK_TEXT_AGENT_DETAIL );
			
		}
		
		public function getDefaultTemplate(){
			$defaultTemplate= get_option(wordpressIDXVirtualPageHelper::OPTION_VIRTUAL_PAGE_TEMPLATE_DEFAULT);
			return $defaultTemplate ;
		}
		

	}//end class
}
?>