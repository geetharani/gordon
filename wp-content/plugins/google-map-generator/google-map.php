<?php 
/**
 * @package Google Map
 * @author Shaon
 * @version 1.3.1
 */
/*
Plugin Name: Google Map
Plugin URI: http://www.intelisoftbd.com/open-source-projects/google-map-generator.html
Description: Using this plugin you can embed google map inside the post or link google map with a link as a popup. example: embed google map <code>{gmap|address|width|height}</code> , popup google map <code>&lt;a href="{gmap_link|address|width|height}"&gt;Link Label&lt;/a&gt;</code> . <br>Other plugins: <a target=_blank href="http://www.intelisoftbd.com/open-source-projects/google-analytics.html">Google Analytics for wordpress</a> | <a href="http://www.intelisoftbd.com/open-source-projects/download-manager-wordpress-plugin.html"  target=_blank >Downloadable Files Manager</a>
Author: Shaon
Version: 1.3.1
Author URI: http://www.wpeden.com
*/
//{gmap|address|width|height}
//{gmap_link|address|width|height}

if($_GET['address']!=''){
        echo showMap($_GET[address], $_GET[width], $_GET[height]);
        die();
    }
    

function googleMap($content){
    
    preg_match_all("/\{gmap\|([^\}]+)\}/", $content, $matches);    
    for($i=0;$i<count($matches[1]);$i++){
    $d = explode("|", $matches[1][$i]);    
    $matches[1][$i] = showMap($d[0],$d[1], $d[2]); // '<iframe width="'.$d[1].'" height="'.$d[2].'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$d[0].'&amp;ie=UTF8&amp;hq=&amp;hnear='.$d[0].'&amp;z=11&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q='.$d[0].'&amp;ie=UTF8&amp;hq=&amp;hnear='.$d[0].'&amp;z=11" style="color:#0000FF;text-align:left">View Larger Map</a></small>';
    }
    
    $content = str_replace($matches[0],$matches[1], $content);
    
    $matches = array();
    preg_match_all("/\{gmap_link\|([^\}]+)\}/", $content, $matches);
    if(count($_GET)>0) $url = $_SERVER['REQUEST_URI'].'&';
    else  $url = $_SERVER['REQUEST_URI'].'?';
    for($i=0;$i<count($matches[1]);$i++){
    $d = explode("|", $matches[1][$i]);    
    $matches[1][$i] = "javascript:window.open('".$url."address=".$d[0]."&width=".$d[1]."&height=".$d[2]."','Google Map for $d[0]','menubar=no,width=".($d[1]+20).",height=".($d[2]+30).",toolbar=no');void(0);";
    }
    
    $content = str_replace($matches[0],$matches[1], $content);    
    return $content;
}

function showMap($address, $w, $h){
    return '<iframe width="'.$w.'" height="'.$h.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$address.'&amp;ie=UTF8&amp;hq=&amp;hnear='.$address.'&amp;z=11&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q='.$address.'&amp;ie=UTF8&amp;hq=&amp;hnear='.$address.'&amp;z=11" style="color:#0000FF;text-align:left">View Larger Map</a></small>';
}

add_filter("the_content","googleMap");