=== Google Map Generator ===
Contributors: Shaon
Donate link: 
Tags: google, map
Requires at least: 2.0.2
Tested up to: 3.1
Stable tag: 4.3

This is a google map generator plugin. Need to embed code {gmap|address|width|height}

== Description ==

Using this plugin you can embed google map inside the post or link google map with a link as a popup. example: embed google map <code>{gmap|address|width|height}</code> , popup google map <code>&lt;a href="{gmap_link|address|width|height}"&gt;Link Label&lt;/a&gt;</code> . 

eaxmple: {gmap|Athens, Greece|400|200}
or
&lt;a href="{gmap_link|Athens, Greece|400|200}"&gt;Map of Athens, Greece&lt;/a&gt;

== Installation ==


1. Upload `google-map.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Frequently Asked Questions ==

= Where to use it? =

Can be used with post and page contents.
eaxmple: 
Embed in post {gmap|Athens, Greece|400|200}

Link in post &lt;a href="{gmap_link|Athens, Greece|400|200}"&gt;Map of Athens, Greece&lt;/a&gt;

== Screenshots ==
N/A

== Changelog ==

= 1.3 =
* adjusted plugin for wp 3.1

= 1.2.3 =
* Some minor bug fixed

= 1.2 =
* new feature: added link map option to show map with a popup

= 1.1 =
* Fixed the conflict with some other plugin

= 1.0 =
* height/width variable added.

== Arbitrary section ==
N/A

== A brief Markdown Example ==
N/A
== Upgrade Notice ==
N/A