<?php

/*
Plugin Name: Google Maps in Posts
Plugin URI: http://www.blog-ware.com
Description: This is New Google Maps in Posts plugin.
Author: www.blog-ware.com
Version: 1.5.3
Author URI: http://www.blog-ware.com
*/

class wpGoogleMapss
{
    /**
     * We check if there are any maps on the page, and store this as a bool here
     *
     * @var bool
     */
    private $isMap;

    /**
     * Current map being added to the page.  This gives unique ids to each map,
     * allowing multiple maps per page
     *
     * @var int
     */
    private $mapNum = 0;

    /**
     * Full url to the Google Maps JavaScript file (including API key)
     *
     * @var string
     */
    private $mapApiUrl;

    /**
     * Google Maps API Key
     *
     * @var string
     */
    private $googleKey;

    /**
     * Gets Google API Key, and creates the url to the Google Maps JavaScript
     *
     */
    public function __construct()
    {$this->addKey();
        $this->googleKey = get_option('wpGoogleMaps_api_key');
        $this->mapApiUrl = "http://maps.google.com/maps?file=api&amp;v=2&amp;key={$this->googleKey}";
		$this->createDB();
		
        $jsDir = get_option('siteurl') . '/wp-content/plugins/google-maps-in-posts/js/';

        wp_register_script('googleMaps', $this->mapApiUrl, false, 2);
        wp_register_script('wpGoogleMaps', "{$jsDir}wp-google-maps.js", array('prototype', 'googleMaps'), '0.0.1');
        wp_register_script('wpGoogleMapsAdmin', "{$jsDir}wp-google-maps-admin.js", array('jquery'), '0.0.2');
    }

    /**
     * Add our plugin configuration menu in the admin section
     * Add boxes to the "edit post" and "edit page" pages
     */
    public function adminMenu()
    { 
        add_meta_box('wpGoogleMaps', 'Google Maps', array($this, 'insertForm'), 'post', 'normal');
        add_meta_box('wpGoogleMaps', 'Google Maps', array($this, 'insertForm'), 'page', 'normal');
        if ( function_exists('add_submenu_page') ) {
            $page = add_options_page( __('wpGoogleMaps Configuration'), __('wpGoogleMaps Configuration'), 'manage_options', 'wpGoogleMaps-config', array($this, 'configPage'));
            add_action( 'admin_print_scripts-'.$page, array($this, 'printScripts') );
        }
    }

    /**
     * Create the actual plugin configuration page
     */
    public function configPage()
    {
        $title = __('wpGoogleMaps Configuration');
        if ($message) { ?>
            <div id="message" class="updated fade"><p><?php echo $message; ?></p></div>
<?php } ?>
        <script type='text/javascript' src='/wp-includes/js/prototype.js'></script>
        <script type="text/javascript">
           function iconView(div,icon)
          {
              var pre_def = document.getElementById("icon_pre_def");
              if (pre_def) pre_def.style.display = "none";
              new Ajax.Updater(div, '/wp-content/plugins/google-maps-in-posts/icons/icon.php?icon='+icon);
              icon_img = icon;
          }
        </script>
            <div class="wrap">
                <h2><?php echo $title; ?></h2>
                <form action="" method="post">
                    <?php wp_nonce_field('update-options'); ?>
                    <p>Google Maps will allow you to easily add maps to your posts or pages.</p>
                    <table class="form-table">
                        <tr valign="top">
                            <th scope="row"><?php _e('Google API Key:') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_api_key" id="wpGoogleMaps_api_key" value="<?php echo get_option('wpGoogleMaps_api_key'); ?>" />
                                <p id="wpGoogleMaps_message"></p>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Enter Default Width of Map:') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_width"  value="<?php echo get_option('wpGoogleMaps_width'); ?>" />
                                <?php
                                $loc_width = get_option('wpGoogleMaps_width'); 
                                if(empty($loc_width)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">Your Default Width of Map is empty</p>
                                <?php }else{ ?>
                                    <!--<p style="padding: 0.5em; background-color: rgb(204, 255, 204); font-weight: bold;">Your Location is saved</p>-->
                                <?php } ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Enter Default Height of Map:') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_height"  value="<?php echo get_option('wpGoogleMaps_height'); ?>" />
                                <?php
                                $loc_height = get_option('wpGoogleMaps_height'); 
                                if(empty($loc_width)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">Your Default Height of Map is empty</p>
                                <?php }else{ ?>
                                    <!--<p style="padding: 0.5em; background-color: rgb(204, 255, 204); font-weight: bold;">Your Location is saved</p>-->
                                <?php } ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Choose Default Icon of Map-Marker:') ?></th>
                            <td>
                                <div id="icon_select" style="float:left;">
                                    <select name="wpGoogleMaps_icon" id="wpGoogleMaps_icon" onchange="iconView('icon_view',this.value);">
                                     <?php $loc_icon = get_option('wpGoogleMaps_icon'); 
                                        if(!empty($loc_icon))echo '<option value="'.$loc_icon.'">'.$loc_icon.'</option>'; else echo '<option>--Default Icon--</option>'; ?>
                                        <option value="bus.png">bus.png</option>
                                        <option value="camera.png">camera.png</option>
                                        <option value="coffeehouse.png">coffeehouse.png</option>
                                        <option value="dollar.png">dollar.png</option>
                                        <option value="euro.png">euro.png</option>
                                        <option value="homegardenbusiness.png">homegardenbusiness.png</option>
                                        <option value="plane.png">plane.png</option>
                                        <option value="rail.png">rail.png</option>
                                        <option value="restaurant.png">restaurant.png</option>
                                        <option value="greencirclemarker.png">greencirclemarker.png</option>
                                        <option value="headquarters.png">headquarters.png</option>
                                        <option value="house.png">house.png</option>
                                        <option value="sportvenue.png">sportvenue.png</option>
                                        <option value="ferry.png">ferry.png</option>
                                    </select>
                                </div>
                                <div id="icon_view"></div>
<!--                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_icon"  value="<?php //echo get_option('wpGoogleMaps_icon'); ?>" />-->
                                <?php
                                
                                if(empty($loc_width)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">You don't choose Default Icon of Map-Marker</p>
                                <?php }else{ ?>
                                    <div id="icon_pre_def"><img src="/wp-content/plugins/google-maps-in-posts/icons/<?php echo $loc_icon; ?>"></div>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Enter Your Location (Country, City etc.):') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_location"  value="<?php echo get_option('wpGoogleMaps_location'); ?>" />
                                <?php
                                $loc_option = get_option('wpGoogleMaps_location'); 
                                if(empty($loc_option)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">Your Location is empty</p>
                                <?php }else{ ?>
                                    <!--<p style="padding: 0.5em; background-color: rgb(204, 255, 204); font-weight: bold;">Your Location is saved</p>-->
                                <?php } ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Enter Title of Location:') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_title_location"  value="<?php echo get_option('wpGoogleMaps_title_location'); ?>" />
                                <?php
                                $loc_title_option = get_option('wpGoogleMaps_title_location'); 
                                if(empty($loc_title_option)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">Your Title of Location is empty</p>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php _e('Enter Description of Location:') ?></th>
                            <td>
                                <input type="text" size="40" style="width:95%;" name="wpGoogleMaps_desc_location"  value="<?php echo get_option('wpGoogleMaps_desc_location'); ?>" />
                                <?php
                                $loc_desc_option = get_option('wpGoogleMaps_desc_location'); 
                                if(empty($loc_desc_option)) {?>
                                    <p style="padding: 0.5em; background-color: rgb(255, 204, 204); font-weight: bold;">Your Description of Location is empty</p>
                                <?php } ?>
                            </td>
                        </tr>
                   </table>
                    <p class="submit">
                        <input type="submit" name="Submit" value="<?php _e('Update Options &raquo;'); ?>" />
                    </p>
                    <input type="hidden" name="action" value="update" />
                    <input type="hidden" name="page_options" value="wpGoogleMaps_api_key" />
                </form>
            </div>
<?php
    }

    /**
     * Used to check for maps, and set $this->isMap
     *
     * @param array $posts
     * @return array of posts (unchanged)
     */
    public function findMaps($posts)
    {
        $content = '';
        foreach ($posts as $post) {
            $content .= $post->post_content;
        }
        $this->isMap = (bool)preg_match("/\[googleMap(.*)\]/U", $content);

        return $posts;
    }

    /**
     * If you need to check if there are maps on the current page, use this
     * $wpGoogleMaps->isMap();
     *
     * @return bool
     */
    public function isMap()
    {
        return $this->isMap;
    }

    /**
     * Links to the Google Maps API JavaScript and the wpGoogleMaps JS, but only
     * IF there are maps on this page.
     *
     * @return void
     */
    public function wpHead ()
    {
//        if ($this->isMap) {
        	wp_enqueue_script('wpGoogleMaps');
//        }
    }

    /**
     * Links to the Google Maps API JavaScript and the wpGoogleMaps admin JS
     *
     * @return void
     */
    public function adminHead ()
    {
    	if ($GLOBALS['editing']) {
    		$this->printScripts();
    	}
    }

    public function printScripts () {
    	wp_enqueue_script('wpGoogleMapsAdmin');
    	if ( get_option('wpGoogleMaps_api_key') ) {
    		wp_enqueue_script('googleMaps', false, array('wpGoogleMapsAdmin'));
    	}
    }

    /**
     * Given the attributes and content from the googleMap shortCode, this will
     * return an object that has all the settings in it.
     *
     * @param array $attr - attributes from the shortCode
     * @param string $address - content of the shortCode
     * @return stdClass
     */
    private function getMapDetails($attr, $address)
    {
        if (isset($attr['width']) && ctype_digit($attr['width'])) {
            $attr['width'] .= 'px';
        }
        if (isset($attr['height']) && ctype_digit($attr['height'])) {
            $attr['height'] .= 'px';
        }

        $mapInfo = (object)shortcode_atts(array('name'              => '',
                                                'mousewheel'        => 'true',
                                                'zoompancontrol'    => 'true',
                                                'typecontrol'       => 'true',
                                                'directions_to'     => 'true',
                                                'directions_from'   => 'false',
                                                'width'             => '100%',
                                                'height'            => '400px',
                                                'description'       => ''), $attr);

        array_walk($mapInfo, array($this, 'fixTrueFalse'));
        $mapInfo->address = $address;
        return $mapInfo;
    }

    /**
     * Replaces "true" and "false" (strings) with true and false (bool)
     * Used with array_walk
     *
     * @param mixed &$value
     * @param string $key
     */
    private function fixTrueFalse(&$value, $key) {
        if ($value == 'false') {
        	$value = false;
        } elseif ($value == 'true') {
            $value = true;
        }
    }

    /**
     * Echo a warning into the admin section, if no Google API has been entered
     */
    public function warning() {
        echo "<div id='wpGoogleMaps_warning' class='updated fade-ff0000'><p><strong>"
            .__('Google Maps  is almost ready.')."</strong> "
            .sprintf(__('You must <a href="%1$s">enter your Google API key</a> for it to work.'), "options-general.php?page=wpGoogleMaps-config")
            ."</p></div>";
    }

    /**
     * Adds the form to generate a googleMaps shortcode and send it to the
     * editor.  Default values are blank on purpose.  It helps the JavaScript
     * generate the shortest possible shortCode for the map in question.
     *
     * @return void
     */
    public function insertForm() {
    	
		$pid = $GLOBALS['post']->ID;
		$blog_location = get_option('wpGoogleMaps_location');
		$blog_title_location = get_option('wpGoogleMaps_title_location');
		$blog_desc_location = get_option('wpGoogleMaps_desc_location');
		$blog_width = get_option('wpGoogleMaps_width'); 
		$blog_height = get_option('wpGoogleMaps_height'); 
		$blog_icon = get_option('wpGoogleMaps_icon'); 
		if(empty($blog_location)) $blog_location = 'Germany';
		if(empty($blog_title_location)) $blog_title_location = 'Title of Blog Location';
		if(empty($blog_desc_location)) $blog_desc_location = 'Description of Blog Location';
		if(empty($blog_width)) $blog_width = 500;
		if(empty($blog_height)) $blog_height = 500;
		if(empty($blog_icon)) $blog_icon = 'house.png';
    	if(!empty($pid)){
    		$google_map = $GLOBALS['wpdb']->get_results("SELECT * FROM ".$GLOBALS['table_prefix']."google_maps WHERE pid=$pid ",ARRAY_A);
//	    		var_dump($google_map);
    	}
    	$zoom = $google_map[0]['zoom'];
		if(empty($zoom)) $zoom = 17;
		
		//for save map with post
		$lang = $GLOBALS['wpdb']->get_results("SELECT * FROM ".$GLOBALS['table_prefix']."langs ORDER BY  lang_order ASC", ARRAY_A);
        $def_lang = $lang[0]['lang_shortcode'];
        
?>
           <style type="text/css">
            .table_inner{
            border-bottom:2px solid #CCCCCC;
            border-right:2px solid #CCCCCC;
            }
            .table_inner td{
            padding:5px;
            }
            
            #pluginoverlay {
        			position: absolute;
        			bottom: 50px;
        			left: 0px;
        			z-index: 90;						
        			vertical-align: middle;
        			width: 16px;
        			filter: alpha(opacity=75);
        			-moz-opacity: 0.75;
        			opacity: 0.75; 
        			text-align: center;			
        		}
            </style>
        <script type='text/javascript' src='/wp-includes/js/prototype.js'></script>
        <script type='text/javascript' src='/wp-content/plugins/google-maps-in-posts/js/markermanager.js'></script>
        
        <table width="100%">
            <tr>
                <td width="70%" style="padding:10px;">
                    <table width="100%" bgcolor="#eaf3fa" class="table_inner">
                        <tr>
                            <td colspan="2">
                                <b>Add a placemarker for the location:</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="width:80px;float:left">Address:</div><input type="text" size="30" id="addressTEXT" value="<?php echo $blog_location ?>" />
                            </td>
                            <td>
                                <select name="icon" id="icon" onchange="iconView('icon_view',this.value);">
                                    <option>--Default Icon--</option>
                                    <option value="bus.png">bus.png</option>
                                    <option value="camera.png">camera.png</option>
                                    <option value="coffeehouse.png">coffeehouse.png</option>
                                    <option value="dollar.png">dollar.png</option>
                                    <option value="euro.png">euro.png</option>
                                    <option value="homegardenbusiness.png">homegardenbusiness.png</option>
                                    <option value="plane.png">plane.png</option>
                                    <option value="rail.png">rail.png</option>
                                    <option value="restaurant.png">restaurant.png</option>
                                    <option value="greencirclemarker.png">greencirclemarker.png</option>
                                    <option value="headquarters.png">headquarters.png</option>
                                    <option value="house.png">house.png</option>
                                    <option value="sportvenue.png">sportvenue.png</option>
                                    <option value="ferry.png">ferry.png</option>
                                </select>
                                <div id="icon_view"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="width:80px;float:left">Title:</div><input type="text" size="60" id="infoWindowTITLE" value="<?php echo $blog_title_location ?>"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="width:80px;float:left">Description:</div><textarea style="width:250px;height:70px;" id="infoWindowTEXT" ><?php echo $blog_desc_location ?></textarea>
                            </td>
                            <td>
                                <p class="submit" style="padding: 0px; border-top:0px solid gray; margin-top: 0px;">
                                    <input type="button" value="Place a marker" onclick="showAddress()"/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:0px;">
                                <hr style="margin:0px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <b>Add a Blank placemarker for any location:</b> 
                            </td>
                            <td>
                                <p class="submit" style="padding: 0px; border-top:0px solid gray; margin-top: 0px;">
                                    <input type="button" value="Place a marker" onclick="centerMarker();setTimeout(function(){jQuery('#map_canvas>div>div>div>.gmnoprint>img').hide();},300);"/>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align:middle;text-align:center;">
                    <table width="100%" bgcolor="#eaf3fa" class="table_inner">
                        <tr>
                            <td colspan="2">
                                <b>Map Settings</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Map Width:
                            </td>
                            <td style="text-align:left;">
                                <input type="text" size="4" name="wpGoogleMaps[width]" id="wpGoogleMaps_width" value="<?php if(!empty($google_map)) echo $google_map[0]['width']; else echo $blog_width; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Map Height:
                            </td>
                            <td style="text-align:left;">
                                <input type="text" size="4" name="wpGoogleMaps[height]" id="wpGoogleMaps_height" value="<?php if(!empty($google_map)) echo $google_map[0]['height']; else echo $blog_height; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Zoom: <a href="javascript:setInMap();"><img src="/wp-content/plugins/google-maps-in-posts/images/in_zoom.png" alt="In Zoom"></a>&nbsp;&nbsp;
                                <input type="text" name="wpGoogleMaps[zoom]" id="wpGoogleMaps_zoom" value="<?php if(!empty($google_map[0]['zoom'])) echo $google_map[0]['zoom']; else echo '17'; ?>" size="1" readonly>
                                &nbsp;&nbsp;<a href="javascript:setOutMap();"><img src="/wp-content/plugins/google-maps-in-posts/images/out_zoom.png" alt="Out Zoom"></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                      <center><div id="map_canvas" style="width: 600px; height: 400px;"></div></center>
                </td>
                <td>
                    <table width="100%">
                        <tr>
                            <td >
                                <div id="map_sidebar" style="width: 300px; height: 300px; background-color:#eaf3fa;padding:5px;border-bottom:2px solid #CCCCCC;border-right:2px solid #CCCCCC;">
                                    <h2 style="margin:0px;">Marked locations</h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="submit" style="padding: 0px; border-top:0px solid gray;">
                                   <input type="button" onclick="showMessageInsert();" value="<?php _e('Publish map in post &raquo;'); ?>" />
                                   <input type="submit" value="<?php _e('Save the map and post &raquo;'); ?>" id="save-post" name="save"/>
                                 </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="map_mess" style="margin: 3px; padding: 3px ; width: 490px; height: 60px; float: left;"></div>
                </td>
                <td>
                    <a href="http://maps.google.de/support/" class="submit" target="_blank" style="padding: 0px; border-top:0px solid gray; margin-top: 0px;">Google Maps Help Center</a>
                </td>
            </tr>
        </table>
        <div id="googleMapMarker" style="display:none;">
        
        </div>
        
        
        
        
        
        
        
        
        
        <!--
        <div style="width:850px;height:370px;">
             
             
             
             <p class="submit" style="padding: 0px; border-top:0px solid gray;">
               
             </p>
        </div>
        
        <p class="submit" >
            <input type="button" onclick="setTimeout(function(){setFocus(edCanvas2.id+'_ifr')},1000);return wpGMapsAdmin.sendToEditor(this.form);" value="<?php //_e('Send Map to Editor &raquo;'); ?>" />
            
        </p>-->
<!--        <p id="wpGoogleMaps_message">&nbsp;</p>-->
        
<!--        <input type="button" value="111" onclick="get();">-->
        
        
        
        <script type="text/javascript">
        function iconView(div,icon)
          {
            new Ajax.Updater(div, '/wp-content/plugins/google-maps-in-posts/icons/icon.php?icon='+icon);
            icon_img = icon;
          }
                  
                  
        var address_location = '<?php echo $blog_location; ?>';
        var mapCount = 0;
        var mapMarkers = new Array();
        var mapTitle = new Array();
        var mapHTMLS = new Array();
        var mapPoints = new Array();
        var icon_img = 0;
        var zoom = 17;    
            function setInMap(zoom){
                map.zoomIn();
            }
            function setOutMap(zoom){
                map.zoomOut();
            }
            
            function showMessageInsert(){
                var obj = document.getElementById("map_mess");
                obj.style.border = "2px solid #e9584c";
                obj.style.backgroundColor = "#eaf3fa";
                obj.innerHTML = "<b>If you want add map to post, you must insert this text: <pre style='margin:5px;'>[googleMap]map[/googleMap]</pre> on place in post where map will be display</b>";
            }
            
            function saveMapToPost(){
                var t = document.getElementById("<?php echo $def_lang; ?>_title").value;
                if(t.length>0){
                    document.post.submit();
                }else{
                    alert('First You must enter Title of Post!');
                }
            }
            
//          function initialize() {
            if (GBrowserIsCompatible()) {
              map = new GMap2(document.getElementById("map_canvas"));
              map.setCenter(new GLatLng(37.4419, -122.1419), <?php echo $zoom; ?>);
              map.addControl(new GLargeMapControl());
              map.enableGoogleBar();
              GEvent.addListener(map,"zoomend", function(oldLevel, newLevel)
                     {
                      document.getElementById("wpGoogleMaps_zoom").value = newLevel;
                     }); 
              geocoder = new GClientGeocoder();
              
              geocoder.getLatLng(
		                address_location,
		                function(point) {
		                    if (!point) {
		                      alert(address + " not found");
    		                } else {
    		                    map.setCenter(point, document.getElementById("wpGoogleMaps_zoom").value);
    		                     <?php if(empty($google_map)){ ?>
    		                    icon_img = '<?php echo $blog_icon; ?>';
                                var marker_obj = showMarket(map.getCenter(),mapCount);
                                mapMarkers.push(marker_obj); 
                                mapPoints.push(map.getCenter()); 
                                var myHtml = '<span style="font-weight:bold;font-size:15px;"><?php echo $blog_title_location; ?></span><br><?php echo $blog_desc_location; ?>';                                
                                mapTitle.push('<?php echo $blog_title_location; ?>');
                                mapHTMLS.push('<?php echo $blog_desc_location; ?>');
                                createMarkerHidden(mapCount,0);
                                map.openInfoWindow(map.getCenter(), myHtml);
                                createMarkerSidebar(mapCount,'infoWindowTITLE','infoWindowTEXT',0);
                                ++mapCount;
                                icon_img = 0;
                                 <?php } ?>
    		                }
		                });
              
//              mgr = new MarkerManager(map, {trackMarkers:true});
//              
//            }
          }
//          initialize();
          
          function showAddress() {
            var address = document.getElementById("addressTEXT").value;
            var title = document.getElementById("infoWindowTITLE").value;
            var text = document.getElementById("infoWindowTEXT").value;
            if (title.length>0) {
            	if (text.length>0) {
		            if (geocoder) {
		              geocoder.getLatLng(
		                address,
		                function(point) {
		                  if (!point) {
		                    alert(address + " not found");
		                  } else {
		                    map.setCenter(point, document.getElementById("wpGoogleMaps_zoom").value);
		                    var icon_ins = getIcon();
		                    marker = new GMarker(map.getCenter(), {draggable:true, icon: icon_ins});
		                    var cau = mapCount;
		                    var poi = marker.getPoint();
		                    createMarkerSidebar(mapCount,'infoWindowTITLE','infoWindowTEXT');
		                    marker.bindInfoWindowHtml('<span style="font-weight:bold;font-size:15px;">'+mapTitle[mapCount]+'</span><br>'+mapHTMLS[mapCount]);
		          	        GEvent.addListener(marker, 'click', function() {
		                      mapMarkers[mapCount].openInfoWindowHtml('<span style="font-weight:bold;font-size:15px;">'+mapTitle[mapCount]+'</span><br>'+mapHTMLS[mapCount]);
		                    });
		          	        GEvent.addListener(marker, 'dragend', function() {
		                      changePoint(cau, poi, 1);
		                    });
		                    map.addOverlay(marker);
		                    mapMarkers.push(marker);
		                    mapPoints.push(map.getCenter());
		                    createMarkerHidden(mapCount,0);
		                    ++mapCount;
		                  }
		                }
		              );
		            }
		         }else{
			    	alert('You must enter Text of Marker!!!');
			    }
		    }else{
		    	alert('You must enter Title of Marker!!!');
		    }
          }
          function hideKrest(){
                jQuery("#map_canvas>div>div>div>.gmnoprint>img").hide();
          }
//          function get(){
////          	alert(map.getCenter());
//          }
          function showMarket(point,mapCount) {
                   var icon_ins = getIcon();
                    marker = new GMarker(map.getCenter(), {draggable:true, icon: icon_ins});
//                    marker.bindInfoWindowHtml(mapHTMLS[mapCount]);
          	        GEvent.addListener(marker, 'click', function() {
          	          mapMarkers[mapCount].openInfoWindowHtml('<span style="font-weight:bold;font-size:15px;">'+mapTitle[mapCount]+'</span><br>'+mapHTMLS[mapCount]);
                    });
          	        GEvent.addListener(marker, 'dragend', function() {
          	            changePoint(mapCount,marker.getPoint(),0);
                    }); 
                    map.addOverlay(marker);
                    
                    
                    return marker;
          }
          
          function centerMarker(){
                var point=map.getCenter(); 
                var marker_obj = showMarket(map.getCenter(),mapCount);
                mapMarkers.push(marker_obj); 
                mapPoints.push(map.getCenter()); 
                
                var myHtml = "Add Title for marker:<br><input type='text' id='nogeoTitle' name='nogeoTitle'><br>Add Description for marker:<br><textarea id='description_market'></textarea><br><input type='button' value='Save' onClick=createMarkerSidebar("+mapCount+",'nogeoTitle','description_market',1);map.closeInfoWindow();><input type='button' value='Cancel' onClick=map.closeInfoWindow();map.removeOverlay(mapMarkers["+mapCount+"]);mapMarkers.pop();mapPoints.pop();>";
                
                map.openInfoWindow(map.getCenter(), myHtml);
                
          }
          
          function getIcon() {
            if (icon_img!=0) {
                icon = new GIcon();
                icon.image = '/wp-content/plugins/google-maps-in-posts/icons/'+icon_img;
                icon.iconSize = new GSize(32, 32);
                icon.iconAnchor = new GPoint(16, 16);
                icon.infoWindowAnchor = new GPoint(25, 7);
                icon_go = icon;
                return icon_go;
            }
          }
          
          function markerClicked(markerNum) {
            mapMarkers[markerNum].openInfoWindowHtml('<span style="font-weight:bold;font-size:15px;">'+mapTitle[markerNum]+'</span><br>'+mapHTMLS[markerNum]);
          }
          function markerClickedReturn(markerNum) {
            return mapMarkers[markerNum].openInfoWindowHtml('<span style="font-weight:bold;font-size:15px;">'+mapTitle[markerNum]+'</span><br>'+mapHTMLS[markerNum]);
          }
          
          function createMarkerSidebar(marker,title,text,nogeo) {
          	
          	var div = document.createElement("div");
          	div.setAttribute("id","wpGoogleMaps_"+marker+"_a");
          	document.getElementById("map_sidebar").appendChild(div);
          	
          	var tex=document.getElementById(text).value;
            var tit=document.getElementById(title).value;
          	
                    var markerA = document.createElement("a");
                    markerA.setAttribute("href","javascript:markerClicked('" + marker +"')");
                    markerA.style.color = "#000000";
                    if (nogeo==1){
                    	 var sidebarText= document.getElementById("nogeoTitle").value;
                    }else if (nogeo==2){
                    	var sidebarText= tit;
                    }else {
                    	var sidebarText= document.getElementById("infoWindowTITLE").value;
                    }
                   
                    markerA.appendChild(document.createTextNode(sidebarText));
                    div.appendChild(markerA);
                    
                    var markerE = document.createElement("a");
                    markerE.setAttribute("href","javascript:markerEdit('" + marker +"')");
                    markerE.style.color = "#000000";
                    var markerI2 = document.createElement("img");
                    markerI2.setAttribute("src","<?php echo get_option('siteurl'); ?>/wp-content/plugins/google-maps-in-posts/icons/edit.png");
                    markerE.appendChild(markerI2);
                    div.appendChild(markerE);
                    
                    var markerR = document.createElement("a");
                    markerR.setAttribute("href","javascript:markerRemove('" + marker +"')");
                    markerR.style.color = "#000000";
                    var markerI = document.createElement("img");
                    markerI.setAttribute("src","<?php echo get_option('siteurl'); ?>/wp-content/plugins/google-maps-in-posts/icons/delete.png");
                    markerR.appendChild(markerI);
                    div.appendChild(markerR);
                    
                    
                    
                    
                    
                    //document.getElementById("map_sidebar").appendChild(document.createElement("br"));
//                    document.getElementById("map_sidebar").appendChild(document.createElement("br"));
                    

                    if (nogeo!=0 && nogeo!=2){
                        mapHTMLS.push(tex);
                        mapTitle.push(tit);
                     }
                    if (nogeo==1){
                        createMarkerHidden(marker,0);
                        ++mapCount;
                     }
          }
          
          function changePoint(count,point,how){ 
              if(how==0){
                  document.getElementById("wpGoogleMaps_"+count+"_point").value = point;
//                  alert(point);
              }
              if(how==1){
                  var mar = mapMarkers[count];
                  document.getElementById("wpGoogleMaps_"+count+"_point").value = mar.getPoint();
//                  alert(mar.getPoint());
              }
              if(how==2){
//                  alert(count);
//                  alert(point); 
                  document.getElementById("wpGoogleMaps_"+count+"_point").value = point;
              }
//              alert(count);
              
              
//              alert(mar.getPoint());
//              document.getElementById("wpGoogleMaps_"+count+"_point").value = point;
          }
          
          function createMarkerHidden(count,icon_in) {
                    var input = document.createElement("input");
                    input.setAttribute("name","wpGoogleMaps[marker]["+count+"][point]");
                    input.setAttribute("id","wpGoogleMaps_"+count+"_point");
                    input.setAttribute("value",mapPoints[count]);
                    input.setAttribute("type","hidden");
                    document.getElementById("googleMapMarker").appendChild(input);
                    
                    var input1 = document.createElement("input");
                    input1.setAttribute("name","wpGoogleMaps[marker]["+count+"][title]");
                    input1.setAttribute("id","wpGoogleMaps_"+count+"_title");
                    input1.setAttribute("value",mapTitle[count]);
                    input1.setAttribute("type","hidden");
                    document.getElementById("googleMapMarker").appendChild(input1);
                    
                    var input2 = document.createElement("input");
                    input2.setAttribute("name","wpGoogleMaps[marker]["+count+"][description]");
                    input2.setAttribute("id","wpGoogleMaps_"+count+"_description");
                    input2.setAttribute("value",mapHTMLS[count]);
                    input2.setAttribute("type","hidden");
                    document.getElementById("googleMapMarker").appendChild(input2);
                    
                    var input3 = document.createElement("input");
                    input3.setAttribute("name","wpGoogleMaps[marker]["+count+"][icon]");
                    input3.setAttribute("id","wpGoogleMaps_"+count+"_icon");
                    if (icon_in==0) {
                        input3.setAttribute("value",icon_img);
                    }else{
                        input3.setAttribute("value",icon_in);
                    }
                    input3.setAttribute("type","hidden");
                    document.getElementById("googleMapMarker").appendChild(input3);
                    
          }
          
           function markerEdit(marker)
          {
            var myHtml = "Add Title for marker:<br><input type='text' id='nogeoTitle' name='nogeoTitle' value='"+mapTitle[marker]+"'><br>Add Description for marker:<br><textarea id='description_market'>"+mapHTMLS[marker]+"</textarea><br><input type='button' value='Save' onClick=updateMarker("+marker+");map.closeInfoWindow();>";
            map.openInfoWindow(mapPoints[marker], myHtml);
          }
           function updateMarker(marker)
          {
          	mapTitle[marker] = document.getElementById('nogeoTitle').value;
          	mapHTMLS[marker] = document.getElementById('description_market').value;
          	var tit = document.getElementById('wpGoogleMaps_'+marker+'_title');
          	var tex = document.getElementById('wpGoogleMaps_'+marker+'_description');
          	tit.setAttribute("value",mapTitle[marker]);
          	tex.setAttribute("value",mapHTMLS[marker]);
          }
          
           function markerRemove(marker)
          {
          	 mapMarkers[marker].hide();
          	 document.getElementById("wpGoogleMaps_"+marker+"_a").style.display = "none";
          	 document.getElementById("googleMapMarker").removeChild(document.getElementById("wpGoogleMaps_"+marker+"_point"));
          	 document.getElementById("googleMapMarker").removeChild(document.getElementById("wpGoogleMaps_"+marker+"_title"));
          	 document.getElementById("googleMapMarker").removeChild(document.getElementById("wpGoogleMaps_"+marker+"_description"));
          	 document.getElementById("googleMapMarker").removeChild(document.getElementById("wpGoogleMaps_"+marker+"_icon"));
          }
          <?php
            
				if(!empty($google_map)){
					$google_map[0]['markers'] = unserialize($google_map[0]['markers']);
					$google_map[0]['markers'] = unserialize(base64_decode($google_map[0]['markers'][0]));
					$zoom = $google_map[0]['zoom'];
					if(empty($zoom)) $zoom = 17;
//					var_dump($google_map);
					echo  "
							     function initpoint(){
							     ";
									foreach ($google_map[0]['markers'] as $key=>$item)
									{
										echo "if ('".$item['icon']."'!=0) {
									                icon = new GIcon();
									                icon.image = '/wp-content/plugins/google-maps-in-posts/icons/".$item['icon']."';
									                icon.iconSize = new GSize(32, 32);
									                icon.iconAnchor = new GPoint(16, 16);
									                icon.infoWindowAnchor = new GPoint(25, 7);
									                icon_go = icon;
									                marker_".$key." = new GMarker(new GLatLng".$item['point'].",{draggable:true,icon: icon_go});
									            }else {
									            	marker_".$key." = new GMarker(new GLatLng".$item['point'].",{draggable:true});
									            }			
									            var cau_".$key." = mapCount;
//									            var poi = marker.getPoint();
								            	GEvent.addListener(marker_".$key.", 'click', function() {
        						          	          map.openInfoWindowHtml(new GLatLng".$item['point'].", '<span style=".'"'."font-weight:bold;font-size:15px;".'"'.">".stripslashes($item['title'])."</span><br>".stripslashes($item['description'])."' );
        						          	       });
        						          	    GEvent.addListener(marker_".$key.", 'dragend', function() {
                                      	              changePoint(cau_".$key.", marker_".$key.".getPoint(),2);
                                                   }); 
											    map.addOverlay(marker_".$key.");
  												mapMarkers.push(marker_".$key.");
                       						    mapPoints.push(new GLatLng".$item['point'].");
                       						    mapHTMLS[mapCount]='".stripslashes($item['description'])."';
            							        mapTitle[mapCount]='".stripslashes($item['title'])."';
                    							if ('".$item['icon']."'==0) {	
    									            createMarkerHidden(mapCount,0);
                    							}else{
                    							    createMarkerHidden(mapCount,'".$item['icon']."');
                    							}
								                createMarkerSidebar(mapCount,'wpGoogleMaps_'+mapCount+'_title','wpGoogleMaps_'+mapCount+'_description',2);
							                    ++mapCount;
									            ";
									}
					echo "         }
        					       if (GBrowserIsCompatible()) {
            					      initpoint();
            					      map.setZoom(".$zoom.");
            					    }
					      
					      
					";
				}
          ?>
        </script>
<?php
    }

    /**
     * Replace our shortCode with the necessary divs (one for the map, and one
     * for directions) and some JavaScript to start the map
     *
     * @param array $attr - array of attributes from the shortCode
     * @param string $content - Content of the shortCode
     * @return string - formatted XHTML replacement for the shortCode
     */
    public function handleShortcodes($attr, $content='')
    {
        $this->mapNum++;
        $mapInfo = $this->getMapDetails($attr, $content);
        $pid = $GLOBALS['post']->ID;
         $google_map = $GLOBALS['wpdb']->get_results("SELECT * FROM ".$GLOBALS['table_prefix']."google_maps WHERE pid=$pid ",ARRAY_A);
         $google_map[0]['markers'] = unserialize($google_map[0]['markers']);
				$google_map[0]['markers'] = unserialize(base64_decode($google_map[0]['markers'][0]));
        if (function_exists('json_encode')) {
        	$json = json_encode($google_map[0]);
        } else {
			require_once('json_encode.php');
        	$json = Zend_Json_Encoder::encode($google_map[0]);
		}
//		echo "<pre>";
//print_r($google_map);
//echo "</pre>"; 
$width = $google_map[0]['width'];
$height = $google_map[0]['height'];
$zoom = $google_map[0]['zoom'];
if(empty($zoom)) $zoom = 17;

		$text = "
<div id='map_".$google_map[0]['pid']."' style='width:{$google_map[0]['width']}px; height:{$google_map[0]['height']}px;' class='googleMap'></div>

<script type='text/javascript'>
//<![CDATA[
var points = new Array();";
		foreach ($google_map[0]['markers'] as $key=>$item){
        $text .= 'var poin = new GLatLng'.$item['point'].' ;';
        $text .= 'points.push(poin);';
		}

		
		
$text .= "
var numpoints = points.length - 1;
      var points2 = points.slice();
    points2.sort(function(p1, p2) {
      return p1.lng() - p2.lng();
    });
    var west = points2[0].x;
    var east = points2[numpoints].x;
    points2.sort(function(p1, p2) {
      return p1.lat() - p2.lat();
    });
    var north = points2[numpoints].y;
    var south = points2[0].y;
    var sw = new GLatLng(south,west);
    var ne = new GLatLng(north,east);
    var bounds = new GLatLngBounds(sw, ne);
    var centerpoint = new GLatLng((north + south)/2, (east + west)/2);
    
    
    

    jQuery(document).ready(function(){
                                  jQuery('#map_".$google_map[0]['pid']."').append('<div style=\'position: absolute;	bottom: 50px;	left: 0px;	z-index: 90;vertical-align: middle;	width: 16px;filter: alpha(opacity=75);-moz-opacity: 0.75;	opacity: 0.75; text-align: center;	\' id=\'pluginoverlay\'><a title=\'Wordpress plugin by www.blog-ware.com\' href=\'http://blog-ware.com\' target=\'_blank\'><img src=\'/wp-content/plugins/google-maps-in-posts/images/pluginbanner.png\' alt=\'Wordpress plugin by www.blog-ware.com\'></a></div>');
                                  });

if (GBrowserIsCompatible()) {
     map = new GMap2(document.getElementById('map_".$google_map[0]['pid']."'));
     geocoder = new GClientGeocoder();
     map.addControl(new GLargeMapControl());
     var zoomlevel = map.getBoundsZoomLevel(bounds);
     map.setCenter(centerpoint, zoomlevel);
     function init(){
     ";
		foreach ($google_map[0]['markers'] as $key=>$item)
		{
//			$text .= "
//					map.setCenter(new GLatLng".$item['point'].",".$zoom.");";
			$text .= "			
					if ('".$item['icon']."'!=0) {
		                icon = new GIcon();
		                icon.image = '/wp-content/plugins/google-maps-in-posts/icons/".$item['icon']."';
		                icon.iconSize = new GSize(32, 32);
		                icon.iconAnchor = new GPoint(16, 16);
		                icon.infoWindowAnchor = new GPoint(25, 7);
		                icon_go = icon;
		                marker = new GMarker(new GLatLng".$item['point'].",{icon: icon_go});
		            }else {
		            	marker = new GMarker(new GLatLng".$item['point'].");
		            }			
		            	GEvent.addListener(marker, 'click', function() {
          	          			map.openInfoWindowHtml(new GLatLng".$item['point'].", '<span style=".'"'."font-weight:bold;font-size:15px;".'"'.">".stripslashes($item['title'])."</span><br>".stripslashes($item['description'])."' );
          	          			});
						map.addOverlay(marker);
					
		            ";
		}
		$text .= "}
		            init();}
		//]]>
		</script>
		";

        return $text;
    }

    
    
    
    function SavePost($id, $postArray = null){
			$id = isset($id) ? $id : $_POST['post_ID'];
			if (!empty($id) && !empty($_POST['wpGoogleMaps']['marker']) && !empty($_POST['wpGoogleMaps']['width']) && !empty($_POST['wpGoogleMaps']['height'])){
          $time = time();
          if(!empty($_POST['wpGoogleMaps']['marker'])){
            foreach ($_POST['wpGoogleMaps']['marker'] as $key=>$item){
              $all_markers[$key] = $item;
              $all_markers[$key]['title'] = addslashes(wordwrap($item['title'] , 50, "<br />"));
              $all_markers[$key]['description'] = addslashes(wordwrap($item['description'] , 40, "<br />"));
            }
          }
          
//    			echo "<pre>";
//    			var_dump($_POST);
//    			var_dump($_POST['wpGoogleMaps']);
//    			var_dump($all_markers);
//          echo "</pre>";die;
          $markers =  serialize(array(base64_encode(serialize(unserialize(serialize($all_markers))))));
          $check = $GLOBALS['wpdb']->get_var("SELECT date_add FROM ".$GLOBALS['table_prefix']."google_maps WHERE pid = $id");
          if(empty($check)){
          	$GLOBALS['wpdb']->query("Insert into ".$GLOBALS['table_prefix']."google_maps (pid,markers,width,height,zoom,date_add) 
                                     VALUES (".$id.", '$markers' , ".$_POST['wpGoogleMaps']['width'].",".$_POST['wpGoogleMaps']['height'].",  ".$_POST['wpGoogleMaps']['zoom'].", $time)");
          }else {
          	$GLOBALS['wpdb']->query("UPDATE ".$GLOBALS['table_prefix']."google_maps SET
          							 markers='$markers',width=".$_POST['wpGoogleMaps']['width'].",height= ".$_POST['wpGoogleMaps']['height'].",zoom= ".$_POST['wpGoogleMaps']['zoom']."
                                     WHERE pid = $id");
          }
          
			}
//      die;
    }
    
    
    function createDB()
    {
    	$sql = 'CREATE TABLE IF NOT EXISTS `'.$GLOBALS['table_prefix'].'google_maps` (
				  `pid` int(32) NOT NULL,
				  `markers` longtext NOT NULL,
				  `width` int(32) NOT NULL,
				  `height` int(32) NOT NULL,
				  `zoom` int(32) NOT NULL,
				  `date_add` int(32) NOT NULL
				);';
    	$GLOBALS['wpdb']->query($sql);
    }    
    function addKey()
    {
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_api_key']))
    	update_option('wpGoogleMaps_api_key',$_POST['wpGoogleMaps_api_key']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_location']))
    	update_option('wpGoogleMaps_location',$_POST['wpGoogleMaps_location']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_title_location']))
    	update_option('wpGoogleMaps_title_location',$_POST['wpGoogleMaps_title_location']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_desc_location']))
    	update_option('wpGoogleMaps_desc_location',$_POST['wpGoogleMaps_desc_location']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_width']))
    	update_option('wpGoogleMaps_width',$_POST['wpGoogleMaps_width']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_height']))
    	update_option('wpGoogleMaps_height',$_POST['wpGoogleMaps_height']);
      if ($_POST['Submit'] && !empty($_POST['wpGoogleMaps_icon']))
    	update_option('wpGoogleMaps_icon',$_POST['wpGoogleMaps_icon']);
//    	$sql = 'INSERT INTO '.$GLOBALS['table_prefix'].'options (option_name,autoload) VALUES (wpGoogleMaps_api_key)';
//    	$GLOBALS['wpdb']->query($sql);
    }
    
    
}

// Instantiate our class
$wpGoogleMaps = new wpGoogleMapss();
//var_dump(get_option('wpGoogleMaps_api_key'));
/**
 * Add filters and actions
 */
if ( !get_option('wpGoogleMaps_api_key') && !isset($_POST['submit']) ) {
    // Add the warning notice if the Google API Key isn't set
    add_action('admin_notices', array($wpGoogleMaps, 'warning'));
} else {
    // Process shortCodes and include JavaScript if the Google API Key is set
    add_filter('the_posts', array($wpGoogleMaps, 'findMaps'));
    add_action('wp_print_scripts', array($wpGoogleMaps, 'wpHead'));
    add_shortcode('googleMap', array($wpGoogleMaps, 'handleShortcodes'));
//    add_shortcode('googleMapMarker ', array($wpGoogleMaps, 'handleShortcodesMarkers'));
}
add_filter('admin_print_scripts', array($wpGoogleMaps, 'adminHead'));
add_action('admin_menu', array($wpGoogleMaps, 'adminMenu'));

add_action('save_post',    array($wpGoogleMaps, 'SavePost'));
add_action('edit_post',    array($wpGoogleMaps, 'SavePost'));
add_action('publish_post',    array($wpGoogleMaps, 'SavePost'));
add_action('publish_page',    array($wpGoogleMaps, 'SavePost'));


//					if (geocoder) 
//					{
//		              geocoder.getLatLng(
//		                'Frankfurt',
//		                function(point) {
//		                  if (!point) {
//		                    alert(address + ' not found');
//		                  } else {
//		                    map.setCenter(point, ".$zoom.");
//		                  }
//		                }
//		              );
//                  }

