=== Google Maps in Posts ===
Contributors: www.blog-ware.com team
Donate link: http://www.blog-ware.com
Tags: maps, google, google maps, shortcodes, placemarker, Google Maps in Posts
Tested up to: 2.6
Stable tag: 1.5.3
Requires at least: 2.2

This plugin allows you to easily insert <a href="http://maps.google.com">Google Maps</a> into your blog, making use of the shortCode system.

== Description ==

Google Maps in Posts plugin for WordPress gives possibility to use Google Maps Services in your blog. 
That could be useful for posts in WP, describing certain locations or events, 
to indicate them immediately in the WP post with a map. The Google Maps Plugin 
gives you a simple and easy Worpress administration back end to handle multiple 
locations and your own location would be defined only once for all the maps of your WP blog.
 

== Plugin Features ==

* Creating your own maps in any post of a WP blog
* Defining the default location (your own business as starting point)
* Default location appears automatically in all the maps on the WP blog
* Different place-markers for different location on the same map
* Each place-marker has icon, title, description 
* Placing a marker for certain location
* Placing a blank marker for any location
* Map settings include width/ height, zoom

Sign up for the [Google Maps API](http://code.google.com/apis/maps/signup.html Google Maps API)

PHP5 Required.

== Installation ==

* Verify that you have PHP5, which is required for this plugin
* Upload the whole `google-maps-in-posts` directory to the `/wp-content/plugins/` directory
* Activate the plugin through the 'Plugins' menu in WordPress
* Enter GoogleMap Api Key, Location, Title of Location and Description of Location in `Plugins/wpGoogleMaps Configuration`
* Create your GoogleMap in post/page

== Frequently Asked Questions ==

= 

Where do I insert a map into my post/page? =



When writing a post or page, there will be a meta box titled "Google Maps in Posts" just below the "Tags" and "Categories" metaboxes.



= Where can I get a Google Maps API Key? =



<a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a>

== Screenshots ==

* screenshot.png

* screenshot2.png

== Changes in version 1.5.3 ==

- bug fix, new url to google Maps Config

== Changes in version 1.5.2 ==

- correction version number
- check automatic plugin update

== Changes in version 1.5.1 ==

- new system centering on the markers

== Changes in version 1.5.0 ==

- replace wpGoogleMaps Configuration in Settings
- fix buq when list posts

== Changes in version 1.4.0 ==

- new design
- fix bug with remove markers

== Changes in version 1.3.0 ==

- fix bug with icons
- add fields(default width, height and icon) in `Plugins/wpGoogleMaps Configuration` for default settings
- fix bug when save post(height and zoom)

== Changes in version 1.2.0 ==

- add zoom element in admin part
- add default marker when create new post/page
- add fields(location, title, description) in `Plugins/wpGoogleMaps Configuration` for default location

== Changes in version 1.1.0 ==

- fix bug (send to translator)